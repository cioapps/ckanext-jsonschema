
import os
import json
import datetime
import logging
import urlparse

from ckan.plugins import toolkit

log = logging.getLogger(__name__)


def _json_load(folder, name):
    '''
    use with caution: the 'folder'
    param is considered trusted (may never be exposed)
    '''
    try:
        file=os.path.realpath(os.path.join(folder,name))
        # ensure it's a file and is readable
        isfile=os.path.isfile(file)
        # ensure it's a subfolder of the project
        issafe = os.path.commonprefix([file, folder]) == folder
        if isfile and issafe:
            with open(file) as s:
                return json.load(s)
        else:
            return None
    except Exception as ex:
        raise Exception("Schema named: {} not found, please check your schema path folder: {}".format(name,str(ex)))

def _find_all_js(root):
    import os
    _dict={}
    for subdir, dirs, files in os.walk(root):
        for filename in files:
            if filename.endswith('.js'):

                key_prefix = ""
                if root != subdir:
                    key_prefix = subdir.replace(root + os.sep, "")

                file_path = os.path.join(key_prefix, filename)
                file_content = open(os.path.join(root, file_path)).read()

                _dict[file_path] = file_content

    return _dict


def _read_all_json(root, prefix=""):
    import os
    _dict={}
    for subdir, dirs, files in os.walk(root):
        for filename in files:
            if filename.endswith('.json'):

                key_prefix = ""
                if root != subdir:
                    key_prefix = subdir.replace(root + os.sep, "")

                key = os.path.join(key_prefix, filename)

                _dict[key]=_json_load(subdir,filename)

    return _dict

def _get_key(filename):  
    return os.path.splitext(filename)[0]


import xmltodict
import pprint
import json

# namespaces = {u'http://www.opengis.net/gml/3.2': u'gml', u'http://www.isotc211.org/2005/srv': u'srv', u'http://www.isotc211.org/2005/gts': u'gts', u'http://www.isotc211.org/2005/gmx': u'gmx', u'http://www.isotc211.org/2005/gmd': u'gmd', u'http://www.isotc211.org/2005/gsr': u'gsr', u'http://www.w3.org/2001/XMLSchema-instance': u'xsi', u'http://www.isotc211.org/2005/gco': u'gco', u'http://www.isotc211.org/2005/gmi': u'gmi', u'http://www.w3.org/1999/xlink': u'xlink'}
# # TODO DEBUG
# import ckanext.jsonschema.utils as _u
# import os
# j = _u.xml_to_json_from_file(os.path.join(_c.PATH_TEMPLATE,'test_iso.xml'))
# import json
# _j=json.loads(j)
# _namespaces=_j['http://www.isotc211.org/2005/gmd:MD_Metadata']['@xmlns']
# namespaces = dict((v,k) for k,v in _namespaces.iteritems())
# _u.json_to_xml()
# _u.xml_to_json_from_file(os.path.join(_c.PATH_TEMPLATE,'test_iso.xml'), True, namespaces)

def xml_to_json_from_file(xml_file, namespaces = None):
    with open(xml_file) as fd:
        return xml_to_json(fd, namespaces = namespaces)

def xml_to_dict(xml_doc, namespaces = None):
    return xmltodict.parse(xml_doc, namespaces = namespaces)


def xml_to_json(xml_doc, namespaces = None):
    # doc = xmltodict.parse(xml_doc, process_namespaces=with_namespace)
    return json.dumps(xml_to_dict(xml_doc, namespaces))

    # pp = pprint.PrettyPrinter(indent=4)
    # return pp.pprint(json.dumps(doc))
    

def json_to_xml(json):
    return xmltodict.unparse(json, pretty=True)

    
def _initialize_license_schema():
    """
    This will break if there is non custom license group, 
    which is if the licenses_group_url key is unset
    """
    
    import ckan.model as model
    import ckanext.jsonschema.constants as _c 


    licenses = model.Package.get_license_options()

    enum_ids = []
    enum_titles = []

    for license in licenses:    
        enum_titles.append(license[0])
        enum_ids.append(license[1])

    data = {
        "enum": enum_ids,
        "options": {
            "enum_titles": enum_titles
        }
    }

    
    path = os.path.join(_c.PATH_SCHEMA, _c.PATH_CORE_SCHEMA)

    if not os.path.exists(path):
        os.makedirs(path)

    file_path = os.path.join(path, "licenses.json")

    with open(file_path, "w") as f:
        f.write(json.dumps(data))

def all_files_in_path_recursive(path, ext):
    import os
    
    all_files = []

    for subdir, dirs, files in os.walk(path):
        for filename in files:
            if filename.endswith(ext):

                # this block gets the relative path starting at <path>
                key_prefix = ""
                if path != subdir:
                    key_prefix = subdir.replace(path + os.sep, "")

                all_files.append(os.path.join(key_prefix, filename))

    return all_files

# This is a default serializer for dicts containing datetimes
def default(obj):
    if isinstance(obj, (datetime.date, datetime.datetime)):
        #return obj.isoformat()
        return obj.strftime("%Y-%m-%dT%H:%M:%SZ")
        # 2002-10-10T17:00:00Z



# def bucket_index_cloudstorage(organization=None, document=None, obj=None, obj_type=None, package=None, bucket_path=None):
#     """
#     Indexes an object (package, resource, or view) in cloud storage, uploads metadata, 
#     and sets the blob's public access settings.

#     :param organization: String representing the organization name (required if no document is provided).
#     :param document: Dictionary containing organization and other metadata (e.g., org_id, res_id, pkg_id) (optional).
#     :param obj: The object to index in cloud storage (package, view, or resource).
#     :param obj_type: A string indicating the type of object being uploaded ('view', 'resource', 'package') (required).
#     :param package: The package object that contains the 'private' flag, needed for privacy status.
#     :param bucket_path: Path to bucket resources (views, packages, resources) (optional).
#     """
#     try:
#         import ckanext.cloudstorage.constants as _cs
#         import ckanext.jsonschema.constants as _c
#         import ckanext.jsonschema.bucket_indexer as _bi

#         # Determine bucket name either from organization or document
#         if document:
#             bucket_name = _cs.PREFIX + document.get("org_name")
#         elif organization:
#             bucket_name = _cs.PREFIX + organization
#         else:
#             raise ValueError("Either 'organization' or 'document' must be provided.")

#         # Determine bucket path based on obj_type if not provided
#         if not bucket_path:
#             if obj_type == 'view':
#                 bucket_path = _c.BUCKET_VIEWS_PATH
#             elif obj_type == 'resource':
#                 bucket_path = _c.BUCKET_RESOURCES_PATH
#             else:
#                 bucket_path = _c.BUCKET_PACKAGES_PATH  # Default to packages path

#         # Initialize the BucketIndexer with the bucket path
#         bucket_indexer = _bi.BucketIndexer(bucket_name, bucket_path)

#         # Check if the bucket exists
#         bucket_exist = bucket_indexer.bucket_helper.check_bucket_exists()

#         if bucket_exist:
#             # Define metadata based on the provided object (whether it's a package, resource, or view)
#             metadata = {
#                 "active": True,
#                 "organization_id": document.get("org_id") if document else None,
#                 "package_id": obj.get('id') if obj_type == 'package' else document.get("pkg_id"),
#                 "resource_id": document.get("res_id") if obj_type == 'resource' else None,
#                 "owner": toolkit.c.user  # Add owner (current user)
#             }

#             # Remove None values from metadata
#             metadata = {k: v for k, v in metadata.items() if v is not None}

#             # Upload the object (package, resource, or view) with metadata
#             bucket_indexer.upload_body(obj, obj.get('id'), metadata)

#             # Set the blob as public or private based on the package's privacy status
#             if package:
#                 package_status = package.get('private', True)
#                 # Set the blob public if the package is not private
#                 bucket_indexer.bucket_helper.set_blob_public(obj.get('id'), not package_status)

#     except Exception as e:
#         log.error("An error occurred while indexing {} in cloud storage: {}".format(obj_type, e))

# To extract id's of views from configuration links ending with ".json"
def extract_id_from_url(url):
    try:
        # Parse the URL
        parsed_url = urlparse.urlparse(url)

        # Check if the URL is valid
        if not parsed_url.scheme or not parsed_url.netloc:
            raise ValueError("Invalid URL")

        # Get the path from the URL and split it
        path = parsed_url.path
        if not path:
            raise ValueError("URL path is empty")

        # Extract the last part of the path and remove the .json extension
        id_with_extension = path.split('/')[-1]

        if not id_with_extension.endswith('.json'):
            raise ValueError("URL does not contain a .json file")

        id_without_extension = id_with_extension.replace('.json', '')
        log.info("item id: {}".format(id_without_extension))

        return id_without_extension

    except ValueError as ve:
        log.error("ValueError: {}".format(ve))
    except Exception as e:
        log.error("An error occurred: {}".format(e))

# def get_view_id_from_item_url(url):
#     length = len(url)
#     return url[length-5-36: length-5]

def fetch_view(url):
    #view_id = get_view_id_from_item_url(url)
    view_id = extract_id_from_url(url)
    return perform_view_show(view_id)


def perform_view_show(view_id):
    try:
        view = toolkit.get_action('jsonschema_view_show')(
            None, {'view_id': view_id, 'resolve': True})
    except Exception as e:
        raise Exception(str(e))
    if view is None:
        raise Exception(
            "The view : {} could not be retrieved for bucket upload".format(view_id))
    return view

def get_current_user():
    username= toolkit.c.user
    user_dict = toolkit.get_action('user_show')(
        data_dict={'id': username})
    return user_dict


def are_dicts_equivalent(dict1, dict2):
  """
  Checks if two dictionaries are equivalent, 
  meaning they have the same keys and values, 
  regardless of the order of key-value pairs.

  Args:
    dict1: The first dictionary.
    dict2: The second dictionary.

  Returns:
    True if the dictionaries are equivalent, False otherwise.
  """

  # Check if both dictionaries have the same number of key-value pairs
  if len(dict1) != len(dict2):
    return False

  # Check if all keys and their corresponding values are the same
  for key, value in dict1.items():
    if key not in dict2 or dict2[key] != value:
      return False

  return True
