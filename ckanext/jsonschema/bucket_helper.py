import os
import time
import logging
from google.cloud import storage, exceptions
import ckan.plugins.toolkit as toolkit
import ckanext.jsonschema.constants as _c
config = toolkit.config

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = _c.GCP_AUTH_PATH

logger = logging.getLogger(__name__)


class UpdateBucketLabelsError(Exception):
    """Custom exception for update bucket labels failures."""
    pass


class BucketHelper():
    def __init__(self, bucket_name, bucket_relative_path):
        self.bucket_name = bucket_name
        self.bucket_relative_path = bucket_relative_path
        try:
            self.storage_client = storage.Client()
            self.bucket = self.storage_client.bucket(self.bucket_name)
        except exceptions.ClientError:
            raise Exception('Bucket helper creation, client error')
        except Exception as e:
            raise Exception('Bucket helper could not be initialized')

    def upload_bucket_file(self, file_str, file_name, metadata=None):
        retry = 0
        while retry < 3:
            try:
                destination_path = self.calculate_destination_path(file_name)
                blob = self.bucket.blob(destination_path)
                # If metadata is provided, set it on the blob before uploading
                if metadata:
                    blob.metadata = metadata
                blob.upload_from_string(file_str)
                # Optionally, you can update metadata after the upload if needed
                if metadata:
                    blob.patch()  # This will save any metadata changes to the blob
                logger.info("The file : {} is successfully uploaded to: {}/{}".format(
                    file_name, self.bucket_name, destination_path))
                return
            except Exception as e:
                retry += 1
                time.sleep(5)
                logger.warn("retry with {}".format(retry))

        raise Exception(
            "The file : {} could not be uploaded to bucket: {} with error: {}".format(file_name, self.bucket_name, str(e)))

    def delete_bucket_file(self, file_name):
        retry = 0
        while retry < 3:
            try:
                destination_path = self.calculate_destination_path(file_name)
                blob = self.bucket.blob(destination_path)
                blob.delete()
                logger.info("The file : {} is successfully deleted at: {}/{}".format(
                    file_name, self.bucket_name, destination_path))
                return
            except exceptions.NotFound:
                logger.info("The file: {} to be deleted from bucket: {} does not exist".format(file_name, self.bucket_name))
                return
            except Exception as e:
                retry += 1
                time.sleep(5)
                logger.warn(
                    "retry with {}".format(retry))
        raise Exception(
                "The file : {} could not be deleted from bucket: {} with error: {}".format(file_name, self.bucket_name, str(e)))

    def calculate_destination_path(self, file_name):
        return "{}/{}".format(self.bucket_relative_path, file_name)


    def check_bucket_exists(self):
        """Check if a GCP bucket exists.

        Returns:
            bool: True if the bucket exists, False otherwise.
        """
        try:
            self.storage_client.get_bucket(self.bucket_name)
            logger.info("Bucket {} exists".format(self.bucket_name))
            return True
        except exceptions.NotFound as e:
            logger.warning("Bucket {} does not exist: {}".format(self.bucket_name, e))
            return False
        except Exception as e:
            logger.error("An error occurred: {}".format(e))
            return False

    def set_blob_public(self, blob_name, make_public=True):
        """Make a blob public or private in a specified bucket.

        Args:
            blob_name (str): The name of the blob (file) in the bucket.
            make_public (bool): If True, make the blob public; if False, make it private.
        """
        # Get the bucket and blob
        try:
            blob_name = blob_name + ".json"
            full_path_blob_name = self.calculate_destination_path(blob_name)
            logger.info('Full path Blob name:  {} '.format(full_path_blob_name))
            blob = self.bucket.blob(full_path_blob_name)
            if make_public:
                # Make the blob publicly accessible
                blob.make_public()
                logger.info('Blob {} in bucket {} is now public.'.format(full_path_blob_name, self.bucket_name))
            else:
                # Remove public access
                blob.acl.all().revoke_read()
                logger.info('Blob {} in bucket {} is now private.'.format(full_path_blob_name, self.bucket_name))
        except exceptions.NotFound:
            logger.error('Error: Bucket {} or blob {} not found.'.format(self.bucket_name, full_path_blob_name))
        except Exception as e:
            logger.error('An error occurred: {}'.format(e))

    #def create_bucket_api_request_url(self, file_name):
    # api_base_url = "https://storage.googleapis.com/storage/v1/b"
    #    file_path = "{}/{}".format(self.bucket_relative_path, file_name)
    #    safe_file_path = urllib.quote(file_path, safe="")
    #    return "{}/{}/o/{}".format(api_base_url, self.bucket_name, safe_file_path)
        # something like that
