from ckan.plugins.interfaces import Interface
from enum import Enum

class IndexOperationType(Enum):
    CREATE = "create"
    DELETE = "delete"
    #UPDATE = "update" # every update is handled as a creation in indexer classes

class ICustomIndexerController(Interface):
    priority = None
    #def custom_index(self, operation, document, package, obj, obj_type):
    #    pass
    def custom_index_package(self, operation, document, package):
       pass

    def custom_index_resource(self, operation, document, package, resource, is_caller_package_index=False):
       pass

    def custom_index_view(self, operation, document, package, view):
       pass
