from ckan.logic.schema import default_show_package_schema
import logging
import hashlib
import json
import ckan.model as model
import ckan.plugins as plugins
from ckan.common import config
import ckan.plugins.toolkit as toolkit
import ckanext.jsonschema.blueprints as _b
import ckanext.jsonschema.configuration as configuration
import ckanext.jsonschema.constants as _c
import ckanext.jsonschema.indexer as _i
import ckanext.jsonschema.logic.action.action as action
import ckanext.jsonschema.tools as _t
import ckanext.jsonschema.validators as _v
import ckanext.jsonschema.view_tools as _vt
from routes.mapper import SubMapper
from ckan.lib.search.__init__ import dispatch_by_operation
from ckan.logic.schema import (default_create_package_schema,
                               default_show_package_schema,
                               default_update_package_schema)
#from ckanext.jsonschema.logic.action.get import reload
from ckanext.jsonschema.logic.actions import (clone_metadata, importer,
                                              validate_metadata, view_list, view_show, 
                                              search_views, search_resources, search_packages)
from ckanext.jsonschema.view_index import ViewSearchIndex
from ckanext.jsonschema.resource_index import ResourceSearchIndex
import ckanext.jsonschema.utils as utils


get_validator = toolkit.get_validator
not_missing = get_validator('not_missing')
not_empty = get_validator('not_empty')
resource_id_exists = get_validator('resource_id_exists')
package_id_exists = get_validator('package_id_exists')
ignore_missing = get_validator('ignore_missing')
empty = get_validator('empty')
boolean_validator = get_validator('boolean_validator')
int_validator = get_validator('int_validator')
OneOf = get_validator('OneOf')
isodate = get_validator('isodate')


convert_to_extras = toolkit.get_converter('convert_to_extras')
convert_from_extras = toolkit.get_converter('convert_from_extras')

# let's grab the default schema in our plugin

log = logging.getLogger(__name__)

view_search_index = ViewSearchIndex()  # index_for(model.ResourceView)
resource_search_index = ResourceSearchIndex()  # index_for(model.Resource)


class JsonschemaPlugin(plugins.SingletonPlugin, toolkit.DefaultDatasetForm):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IConfigurable)
    plugins.implements(plugins.IDatasetForm)
    plugins.implements(plugins.ITemplateHelpers)
    plugins.implements(plugins.IValidators)
    plugins.implements(plugins.IBlueprint)
    plugins.implements(plugins.IActions)
    #plugins.implements(plugins.IRoutes,  inherit=True)
    #plugins.implements(plugins.IResourceController, inherit=True)
    plugins.implements(plugins.IPackageController, inherit=True)
    plugins.implements(plugins.IDomainObjectModification, inherit=True)
    plugins.implements(plugins.IOrganizationController, inherit=True)

    #IRoutes
    def after_map(self, map):
        with SubMapper(map, controller='package') as m:
            m.connect('/dataset/{action}/{id}',
                    requirements=dict(action='|'.join([
                        'new_resource',
                        'history',
                        'read_ajax',
                        'history_ajax',
                        'follow',
                        'activity',
                        'groups',
                        'unfollow',
                        'delete',
                        'api_data',
                    ])), controller='ckanext.jsonschema.controllers:JsonschemaPackageController')

            m.connect('/dataset/{id}/resource_delete/{resource_id}',
                      action='before_delete', controller='ckanext.jsonschema.controllers:JsonschemaResourceController')
        return map

    # IActions
    def get_actions(self):
        actions = {
            'jsonschema_importer': importer,
            #'jsonschema_reload': reload,
            'jsonschema_validate': validate_metadata,
            'jsonschema_clone': clone_metadata,
            'jsonschema_view_show': view_show,
            #'jsonschema_view_search': view_search,
            'jsonschema_search_views': search_views,
            'jsonschema_search_resources': search_resources,
            'jsonschema_search_packages': search_packages,
            'jsonschema_view_list': view_list,
            #'jsonschema_get_all_parents': get_all_parents,

            # CHAINED CKAN ACTIONS
            #'organization_create': action.organization_create,
            'package_delete': action.package_delete,
            'resource_create': action.resource_create,
            'resource_update': action.resource_update,
            'resource_delete': action.resource_delete,
            'resource_view_create': action.resource_view_create,
            'resource_view_update': action.resource_view_update,
            'resource_view_delete': action.resource_view_delete,

        }
        return actions

    # IBlueprint

    def get_blueprint(self):
        return _b.jsonschema

    # ITemplateHelpers

    def get_helpers(self):
        return {
            ######## DEPRECATED ########
            # These are used only on forms to send key - value
            'jsonschema_get_body_key': lambda: _c.SCHEMA_BODY_KEY,
            'jsonschema_get_type_key': lambda: _c.SCHEMA_TYPE_KEY,
            'jsonschema_get_opt_key': lambda: _c.SCHEMA_OPT_KEY,
            ############################

            'jsonschema_as_json': lambda payload: _t.as_json(payload),

            'jsonschema_get_package_body': lambda d: _t.as_dict(_t.safe_helper(_t.get_package_body, d)),
            'jsonschema_get_package_type': _t.get_package_type,
            'jsonschema_get_package_opt': lambda d: _t.as_dict(_t.safe_helper(_t.get_package_opt, d, _c.SCHEMA_OPT)),

            # 'jsonschema_get_resource': lambda r = None : _t.get(r),
            'jsonschema_get_resource_body': lambda r, template = {}: _t.as_dict(_t.safe_helper(_t.get_resource_body, r, template)),
            'jsonschema_get_resource_type': lambda r, default_type = None: _t.safe_helper(_t.get_resource_type, r, default_type),
            'jsonschema_get_resource_opt': lambda r: _t.as_dict(_t.safe_helper(_t.get_resource_opt, r, _c.SCHEMA_OPT)),

            # view helpers
            'jsonschema_get_view_body': lambda r, template = {}: _t.as_dict(_t.safe_helper(_vt.get_view_body, r, template)),
            'jsonschema_get_view_type': lambda r, default_type = None: _t.safe_helper(_vt.get_view_type, r, default_type),
            'jsonschema_get_view_opt': lambda r: _t.as_dict(_t.safe_helper(_vt.get_view_opt, r, _c.SCHEMA_OPT)),
            'jsonschema_url_quote': _t.url_quote,
            'jsonschema_is_jsonschema_view': _vt.is_jsonschema_view,
            # 'jsonschema_get_view_types': _vt.get_view_types,
            'jsonschema_get_configured_jsonschema_types_for_plugin_view': _vt.get_configured_jsonschema_types_for_plugin_view,
            'jsonschema_get_view_info': _vt.get_view_info,
            'jsonschema_get_rendered_resource_view': _vt.rendered_resource_view,

            # DEFAULTS
            'jsonschema_get_schema': lambda x: json.dumps(_t.get_schema_of(x)),
            'jsonschema_get_template': lambda x: json.dumps(_t.get_template_of(x)),
            'jsonschema_get_opt': lambda: _c.SCHEMA_OPT,
            'jsonschema_get_default_license': lambda: _c.DEFAULT_LICENSE,

            'jsonschema_handled_resource_types': configuration.get_supported_resource_types,
            'jsonschema_handled_dataset_types': configuration.get_supported_types,
            'jsonschema_handled_input_types': configuration.get_input_types,
            'jsonschema_handled_output_types': configuration.get_output_types,

            # 'jsonschema_get_resource_label': configuration.get_resource_label
            # jsonschema_get_runtime_opt': lambda x : json.dumps(_t.get_opt_of(x)),
            'jsonschema_get_label_from_registry': _t.get_label_from_registry,

            # FORM CONFIGURATION
            'jsonschema_is_supported_ckan_field': _t.is_supported_ckan_field,
            'jsonschema_is_supported_jsonschema_field': _t.is_supported_jsonschema_field,

        }

    # IPackageContr985oller

    def before_index(self, pkg_dict):
        # IMPORTANT: Check already existing views on not available plugin
        # ckan/lib/datapreview/resource_view_list filters out views for which the plugin is no more available
        # TODO use IBinder to define extension points by plugin
        package = json.loads(pkg_dict['data_dict'])
        package_id = package.get('id')
        organization = pkg_dict.get('organization')
        resources = pkg_dict.get('resources')

        log.info('Indexing package: {}'.format(package_id))
        log.info('organization: {}'.format(organization))

        package_jsonschema_type = _t.get_package_type(package)
        
        if not package_jsonschema_type:
            # do not index jsonschema fields if regular type
            log.info('Jsonschema Indexer may not take care of a not jsonschema package:\
                    skipping jsonschema fields for package {}'.format(package_id))

        else:
            if _t.get_skip_indexing_from_registry(package_jsonschema_type):
                return pkg_dict

            try:
                package_plugin = configuration.get_plugin(
                    package_jsonschema_type)
                pkg_dict = package_plugin.before_index_package(pkg_dict)
            except Exception as e:
                log.error(str(e))

        resources = package.get('resources', [])

        for resource in resources:

            # TODO filter only active resources/views ?

            resource_id = resource.get('id')
            # res_ids.append(resource_id)

            log.info('Indexing resource: {}'.format(resource_id))
            try:
                resource_search_index.index_resource(
                    resource, is_caller_package_index=True)
            except Exception as e:
                log.error(str(e))

            resource_views = toolkit.get_action(
                'resource_view_list')({}, {'id': resource_id})
            for view in resource_views:
                try:
                    view_search_index.index_view(view)
                except Exception as e:
                    log.error(str(e))

        index_id = hashlib.md5(
                '%s%s' % (package_id, config.get('ckan.site_id'))).hexdigest()
        organization = pkg_dict.get('organization')
        document = dict(pkg_dict)
        document.update({
            'index_id': index_id,
            #'id': package_id,
            'site_id': config.get('ckan.site_id'),
            #'title': pkg_dict.get('title'),
            #'entity_type': pkg_dict.get('entity_type'),
            #'dataset_type': pkg_dict.get('dataset_type'),
            #'state': pkg_dict.get('state'),
            #'name': pkg_dict.get('name'),
            #'revision_id': pkg_dict.get('revision_id'),
            #'version': pkg_dict.get('version'),
            #'url': pkg_dict.get('url'),
            'ckan_url': config.get('ckan.site_url'),
            # 'download_url': resources[0].get('url') # deprecated api_version = 1
            #'notes': pkg_dict.get('notes'),
            #'author': pkg_dict.get('author'),
            #'author_email': pkg_dict.get('author_email'),
            #'maintainer': pkg_dict.get('maintainer'),
            #'maintainer_email': pkg_dict.get('maintainer_email'),
            #'license': pkg_dict.get('license_title'),
            #'license_id': pkg_dict.get('license_id'),
            #'ratings_count': pkg_dict.get('ratings_count'),
            #'ratings_average': pkg_dict.get('ratings_average'),
            #'tags':  [tag.get('name') for tag in tags] if tags else [],
            #'groups': pkg_dict.get('groups'),
            #'organization': pkg_dict.get('organization'),
            # to be secure
            #'capacity': 'public' if pkg_dict.get('private') == False else 'private',
            'permission_labels': _t.get_permission_labels(package_id),
            # 'res_name': pkg_dict.get('res_name'),
            #'res_description': pkg_dict.get('res_description'),
            #'res_format': pkg_dict.get('res_format'),
            #'res_format': pkg_dict.get('res_format'),

            # 'status':resource.get('status'),
            #'capabilities': [],
            #'description': pkg_dict.get('description'),

            _c.SCHEMA_TYPE_KEY: _t.get_package_type(pkg_dict),
            _c.SCHEMA_BODY_KEY: json.dumps(_t.get_package_body(pkg_dict)) or None,
            _c.SCHEMA_OPT_KEY: json.dumps(_t.get_package_opt(pkg_dict)),
            #'indexed_ts': datetime.datetime.utcnow().isoformat()
        })
        #if _c.CLOUDSTORAGE_PLUGIN_ENABLED:
        #    utils.bucket_index_cloudstorage(
        #        organization=organization, package=package, obj=document, obj_type="package")
        from ckanext.jsonschema.indexer_controller import IndexOperationType
        _i.handle_custom_indexing(operation=IndexOperationType.CREATE, obj=package, obj_type="package", document=document, package=package)
        log.info('Indexed package: {}'.format(pkg_dict.get('id')))
        return document


    # def before_view(self, pkg_dict):
    #     '''
    #     Extensions will recieve this before the dataset
    #     gets displayed. The dictionary passed will be
    #     the one that gets sent to the template.
    #     '''
    #     pass


    # def _opt_map(self, opt = _c.SCHEMA_OPT, version = _c.SCHEMA_VERSION):
    #     for plugin in _v.JSONSCHEMA_PLUGINS:
    #         try:
    #             opt.update(plugin.opt_map(opt, version))
    #         except Exception as e:
    #             log.error('Error resolving dataset types:\n{}'.format(str(e)))
    #     return opt
    
    # IConfigurer


    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'jsonschema')

        # import ckanext.jsonschema.indexer as indexer
        # indexer.init_core('jsonschema_core')

    # IConfigurable
    def configure(self, config):
        _t.reload()

    # IValidators
    def get_validators(self):

        return {
            u'jsonschema_is_valid': _v.schema_check,
        }

    def is_fallback(self):
        # Return True to register this plugin as the default handler for
        # package types not handled by any other IDatasetForm plugin.
        return False

    # IDatasetForm
    ##############

    def package_types(self):

        package_types = []
        for package_type in configuration.get_supported_types():

            if package_type not in package_types:
                package_types.append(package_type)

        for package_type in configuration.get_input_types():
            if package_type not in package_types:
                package_types.append(package_type)

        return package_types

    # def setup_template_variables(self, context, data_dict):
        # # TODO: https://github.com/ckan/ckan/issues/6518
        # path = c.environ['CKAN_CURRENT_URL']
        # type = path.split('/')[1]
        # jsonschema = {
        #     # 'data_dict' : data_dict
        # }
        # c.jsonschema = jsonschema
        # return jsonschema

    # def new_template(self):
    #     return 'package/snippets/package_form.html'

    # # TODO: https://github.com/ckan/ckan/issues/6518 (related but available on 2.9.x)
    def read_template(self):
        # TODO
        # If you want to render a specific view for a specific jsonschema type, use this method to return the template
        # package_type = _t.get_package_type(toolkit.c.pkg_dict)
        return 'source/read.html'

    # def edit_template(self):
    #     return 'package/snippets/package_form.html'

    # def search_template(self):
    #     return 'package/search.html'

    # def history_template(self):
    #     return 'package/history.html'

    # def resource_template(self):
    #     return 'package/resource_read.html'

    # def package_form(self):
    #     return 'package/new_package_form.html'

    # def resource_form(self):
    #     return 'package/snippets/resource_form.html'

    # Updating the CKAN schema
    def create_package_schema(self):
        schema = default_create_package_schema()
        return _v.modify_package_schema(schema)

    def update_package_schema(self):
        schema = default_update_package_schema()
        return _v.modify_package_schema(schema)

    def show_package_schema(self):
        schema = default_show_package_schema()
        return _v.show_package_schema(schema)

    # IDomainObjectModification
    def notify(self, entity, operation=None):  # change this to work for views here
        entity_type = entity.__class__.__name__
        if isinstance(entity, model.Package):
            #entity = logic.get_action('package_show')(
            #        {'model': model, 'ignore_auth': True, 'validate': False,
            #        'use_cache': False},
            #        {'id': entity.id})
            #entity_type = model.Package

            # it is already being notified by ckan SynchronousSearchPlugin notifier
            return
        if operation != model.domain_object.DomainObjectOperation.deleted:
            dispatch_by_operation(
                entity_type,
                entity,
                operation
                )
            return
        else:
            dispatch_by_operation(entity.__class__.__name__, entity, operation)
            return

        #_is_view = isinstance(entity, model.ResourceView)
        #_is_resource = isinstance(entity, model.Resource)
        # TODO do it for others as well:  _is_resource and _id_organization
        #_is_package = isinstance(entity, model.Package)
        #if not _is_view and not _is_resource and not _is_package:
        #    return
        # the rest might be deleted
        #if operation != model.domain_object.DomainObjectOperation.deleted:
        #    action_name = None
        #    if _is_view:
        #        action_name = 'resource_view_show'
        #    elif _is_resource:
        #        action_name = 'resource_show'
        #    elif _is_package:
        #        action_name = 'package_show'
        #    dispatch_by_operation(
        #        entity.__class__.__name__,
        #        logic.get_action(action_name)(
        #            {'model': model, 'ignore_auth': True, 'validate': False,
        #            'use_cache': False},
        #            {'id': entity.id}),
        #        operation            )
        #else:
        #    log.warn("Discarded Sync. indexing for: %s" % entity)

    # #Deprecated, specific logic for graph indexer
    # #IOrganizationController
    # def create(self, entity):
    #     if not _c.GRAPH_INDEXER_ENABLED:
    #         return
    #     if entity.type == 'organization':
    #         log.info("entity: {}".format(entity))
    #         log.info("entity type: {}".format(type(entity)))
    #         # Extract relevant fields from the entity (Group object)
    #         copied_entity = {
    #             'id': entity.id,
    #             'name': entity.name,
    #             'title': entity.title,
    #             'type': entity.type,
    #             'description': entity.description,
    #             'image_url': entity.image_url,
    #             'created': str(entity.created),
    #             'is_organization': entity.is_organization,
    #             'approval_status': entity.approval_status,
    #             'state': entity.state,
    #             'revision_id': entity.revision_id,
    #         }
    #         copied_entity.update({'item_id': copied_entity.get('id')})
    #         _gi.generic_neo4j_api_post("nodes", [copied_entity])
    #     pass
    #    # This method is not triggered when the creation failed

    # def update(self, entity):
    #     if not _c.GRAPH_INDEXER_ENABLED:
    #         return
    #     if entity.type == 'organization':
    #         log.info("entity: {}".format(entity))
    #         log.info("entity type: {}".format(type(entity)))
    #         # Extract relevant fields from the entity (Group object)
    #         copied_entity = {
    #             'id': entity.id,
    #             'name': entity.name,
    #             'title': entity.title,
    #             'type': entity.type,
    #             'description': entity.description,
    #             'image_url': entity.image_url,
    #             'created': str(entity.created),
    #             'is_organization': entity.is_organization,
    #             'approval_status': entity.approval_status,
    #             'state': entity.state,
    #             'revision_id': entity.revision_id,
    #         }
    #         copied_entity.update({'item_id': copied_entity.get('id')})
    #         _gi.generic_neo4j_api_post("nodes", [copied_entity])
    #     pass

