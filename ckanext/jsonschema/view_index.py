
import json
import logging
import hashlib
import datetime
from ckan.common import config
import ckanext.jsonschema.tools as _t
import ckanext.jsonschema.constants as _c
import ckanext.jsonschema.indexer as _i
import ckanext.jsonschema.bucket_indexer as _bi
import ckanext.jsonschema.view_tools as _vt
import ckanext.jsonschema.utils as utils
from ckanext.jsonschema.indexer_controller import IndexOperationType
import pysolr
import ckan.logic as logic
import ckan.model as model
from ckan.lib.search import SearchIndexError
from ckan.lib.search.index import SearchIndex
from ckan.lib.search.__init__ import _INDICES

logger = logging.getLogger(__name__)

indexer = _i.Indexer(_c.SOLR_VIEWS_CONFIG_PATH, _c.SOLR_VIEWS_BASE_URL,
                     _c.SOLR_VIEWS_CORE_NAME,  _c.SOLR_VIEWS_USERNAME, _c.SOLR_VIEWS_PASSWORD)

# bucket_indexer = _bi.BucketIndexer(
#     _c.BUCKET_NAME, _c.BUCKET_VIEWS_PATH)

class ViewSearchIndex(SearchIndex):
    #implements(IJsonschemaView, inherit=True)
    #implements(IResourceView, inherit=True)

    # SearchIndex
    def __init__(self, *args, **kwargs):
        pass

    def insert_dict(self, data):
        if data is None:
            return

        view = self.model_to_dict(data)
        if self.skip_index(view):
            return
        return self.index_view(view)
    
    def remove_dict(self, view):
        if isinstance(view, model.ResourceView):
            view = self.model_to_dict(view)
        if self.skip_index(view):
            return

        logger.info('Deleting index for view with id {}'.format(view.get('id')))
        self.delete_view(view)

    def update_dict(self, view, defer_commit=False):
        if view is None:
            return

        view = self.model_to_dict(view)
        if self.skip_index(view):
            return
        self.index_view(view, defer_commit)

    def index_view(self, view, defer_commit=False):
        if self.skip_index(view):
            return

        view_id = view.get('id')
        view_type = view.get('view_type')

        package_id = view.get('package_id')
        resource_id = view.get('resource_id')

        try:
            model = _vt.get_model(package_id, resource_id)
        except Exception as e:
            logger.exception(e)
            return

        organization = model['organization']
        package = model['package']
        resource = model['resource']

        logger.info('Indexing view: {}'.format(view_id))
        # logger.info('Dat view: {}'.format(view))

        try:
            document = _before_index_view(organization, package, resource, view)
            ###
            # view_plugin = _vt.get_jsonschema_view_plugin(view_type)

            # if view_plugin:
            #     document = view_plugin.before_index_view(document, organization, package, resource, view)
            # else:
            #     logger.info('No view plugin indexer implementation is found')
            ###

            # self.bucket_index(document, package, view)
            #if _c.CLOUDSTORAGE_PLUGIN_ENABLED:
            #    utils.bucket_index_cloudstorage(document=document, package=package, obj=view, obj_type="view")
            _i.handle_custom_indexing(
                operation=IndexOperationType.CREATE, obj=view, obj_type="view", document=document, package=package)
            # self.bucket_index_cloudstorage(document, package)
            indexer.solr_upload(document)

        except Exception as e:
            logger.error(str(e))

    # def clear(self):# TODO we need to implement clear_index by type
        # clear_index()

    def get_all_entity_ids(self):
        raise NotImplemented

    def commit(self):
        try:
            conn = indexer.get_connection()
            conn.commit(waitSearcher=False)
        except Exception as e:
            logger.exception(e)
            raise SearchIndexError(e)

    def model_to_dict(self, view_model):
        view_dict = logic.get_action('resource_view_show')({'model': model, 'ignore_auth': True, 'validate': False,'use_cache': False},{'id': view_model.id})
        #view_dict = view_model.as_dict()
        #view_dict.update({'package_id': view_model.package_id}) # if we do not do this, we could also delete package_id setter on actions
        return view_dict

    def delete_view(self, view):

        # TODO delete by resource id or package id may be performed if necessary
        #elif view.get('resource_id'):
        #    query = "+res_id:\"%s\"" % (view.get('resource_id'))   #delete views by resource id
        #elif view.get('package_id'):
            # delete views by package id
        #    query = "+pkg_id:\"%s\"" % (view.get('package_id'))

        # if we are sure there will only be one at a time and send solr keys we could do something like:
        #solr_key = view.keys()[0]
        #query = str(solr_key) + ":" + str(view.get(solr_key))

        try:
            view_plugin = _vt.get_jsonschema_view_plugin(view.get('view_type'))
            if view_plugin:
                view_plugin.before_delete_index_view(view)
            else:
                logger.info('No view plugin indexer implementation is found')
        except Exception as e:
            logger.error(str(e))

        view_id = view.get('id')
        #bucket_indexer.delete_body(view_id)
        _i.handle_custom_indexing(operation=IndexOperationType.DELETE, obj=view, obj_type="view")
        # Here the delete should be called for the deleted views

        query = None
        if view_id:
            query = "+id:\"%s\"" % (view_id)  # delete single view

        indexer.delete_dict(query)

    def skip_index(self, view):
        view_jsonschema_type = _vt.get_view_type(view)
        if _t.get_skip_indexing_from_registry(view_jsonschema_type):
            logger.info('Skipping indexing view with jsonschema type: {}'.format(
                view_jsonschema_type))
            return True
        return False

    # def bucket_index(self, document, package, view):

    #     if package.get('private') == False:
    #         operation = _c.UPLOAD_OPERATION_KEY
    #     else:
    #         operation = _c.DELETE_OPERATION_KEY

    #     try:
    #         if operation == _c.UPLOAD_OPERATION_KEY:
    #             bucket_indexer.upload_body(
    #                 view, view.get('id'))
    #         else:
    #             bucket_indexer.delete_body(view.get('id'))

    #     except Exception as e:
    #         logger.error(str(e))

#def clear_index():
#    conn = make_connection()
#    query = "+site_id:\"%s\"" % (config.get('ckan.site_id'))
#    try:
#        conn.delete(q=query)
#    except socket.error as e:
#        err = 'Could not connect to SOLR %r: %r' % (conn.url, e)
#        logger.error(err)
#        raise SearchIndexError(err)
#    except pysolr.SolrError as e:
#        err = 'SOLR %r exception: %r' % (conn.url, e)
#        logger.error(err)
#        raise SearchIndexError(err)

# lets inject in the indexer
_INDICES.update({'resourceview': ViewSearchIndex})

def _before_index_view(organization, pkg_dict, resource, view):
    package_id = pkg_dict.get('id')
    resource_id =  resource.get('id')
    view_id = view.get('id')
    view_type = view.get('view_type')

    view_jsonschema_body = _vt.get_view_body(view)
    view_jsonschema_body_resolved = json.loads(
        json.dumps(view_jsonschema_body))  # deep copy of the body
    view_plugin = _vt.get_jsonschema_view_plugin(view_type)
    if view_plugin:
        try:
            view_jsonschema_body_resolved = view_plugin.resolve(
                view_jsonschema_body_resolved, view)
        except Exception as e:
            logger.error('Error while resolving view.')
            logger.error('Package id:{}, resource id:{}, view id: {}'.format(
                pkg_dict.get('id'), resource.get('id'), view.get('id')))
            logger.error(str(e))

    else:
        logger.warn(
            'No plugin found for view_type: {}'.format(view_type))

    index_id = hashlib.md5(
        '%s%s' % (view['id'], config.get('ckan.site_id'))).hexdigest()
    tags = pkg_dict.get('tags')
    document = {
        'index_id': index_id,
        'site_id' : config.get('ckan.site_id'),
        'capacity': 'public' if pkg_dict.get('private') == False else 'private', # to be safe
        'permission_labels': _t.get_permission_labels(package_id),
        # 'pkg_status': pkg_dict.get('status'),
        #'res_status':resource.get('status'),
        #'status':view,
        'capabilities': [],
        'org_id': organization.get('id'),
        'org_name': organization.get('name'),
        'pkg_id': package_id,
        'pkg_tags':  [tag.get('name') for tag in tags] if tags else [],
        'pkg_name' : pkg_dict.get('name'),
        'pkg_title': pkg_dict.get('title'),
        'pkg_description' : pkg_dict.get('description'),
        'res_id': resource_id,
        'res_type': resource.get('resource_type'),
        'res_name': resource.get('name'),
        'res_description': resource.get('description'),
        'res_format': resource.get('format'),
        'res_url': resource.get('url'),
        'id': view_id,
        'type': view_type,
        'title': view.get('title'),
        'name': view_jsonschema_body_resolved.get('name') if view_jsonschema_body_resolved else None,
        'description': view.get('description'),
        'pkg_' + _c.SCHEMA_TYPE_KEY: _t.get_package_type(pkg_dict), ### ADD THESE TO SCHEMA
        'res_' + _c.SCHEMA_TYPE_KEY: _t.get_resource_type(resource),
        _c.SCHEMA_TYPE_KEY: _vt.get_view_type(view),
        #_c.SCHEMA_TYPE_KEY: _vt.get_view_type(view.config),
        _c.SCHEMA_BODY_KEY: json.dumps(view_jsonschema_body_resolved),
        _c.SCHEMA_OPT_KEY: json.dumps(_vt.get_view_opt(view)) or None,
        #_c.SCHEMA_OPT_KEY: _vt.get_view_opt(view.config) or None
        'indexed_ts': datetime.datetime.utcnow().isoformat()

    }
    # modify dates (SOLR is quite picky with dates, and only accepts ISO dates
    # with UTC time (i.e trailing Z)
    # See http://lucene.apache.org/solr/api/org/apache/solr/schema/DateField.html
    document['indexed_ts'] += 'Z'
    return document