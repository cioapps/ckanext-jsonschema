# try:
#     import pkg_resources
#     pkg_resources.declare_namespace(__name__)
# except ImportError:
#     import pkgutil
#     __path__ = pkgutil.extend_path(__path__, __name__)

from ckanext.jsonschema.indexers.bucket_indexer import JsonschemaBucketIndexer
from ckanext.jsonschema.indexers.event_publisher import JsonschemaEventPublisher
from ckanext.jsonschema.indexers.indexer_utils import create_token
from ckanext.jsonschema.indexers.constants import EVENTARC_TARGETING_PIPELINE
