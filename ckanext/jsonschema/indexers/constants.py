from ckan.plugins import plugin_loaded
import ckan.plugins.toolkit as toolkit
config = toolkit.config

# Name of the eventarc pipeline to activate, it is an attribute called targeting_pipeline of the message
EVENTARC_TARGETING_PIPELINE = config.get('ckanext.jsonschema_event_publisher.targeting_pipeline')


EVENTARC_PROJECT_ID = config.get('ckanext.jsonschema_event_publisher.project_id')
EVENTARC_LOCATION = config.get('ckanext.jsonschema_event_publisher.location')
EVENTARC_MESSAGE_BUS_NAME = config.get('ckanext.jsonschema_event_publisher.message_bus')
EVENTARC_SPEC_VERSION = '1.0'

# try:
#     if plugin_loaded("jsonschema_event_publisher"):
#         EVENT_PUBLISHER_PLUGIN_ENABLED = True
#     else:
#         EVENT_PUBLISHER_PLUGIN_ENABLED = False
# except Exception:
#     EVENT_PUBLISHER_PLUGIN_ENABLED = False

#if EVENT_PUBLISHER_PLUGIN_ENABLED and not EVENTARC_TARGETING_PIPELINE:
if not EVENTARC_TARGETING_PIPELINE:
    raise Exception(
        'ckanext.jsonschema_event_publisher.targeting_pipeline configuration should be set when jsonschema_event_publisher is enabled')

