import os
import logging
import ckanext.jsonschema.utils as _u

logger = logging.getLogger(__name__)

# deprecated
def create_token():
    from google.auth.transport.requests import Request
    from google.oauth2 import service_account

    # from ckanext.jsonschema.constants import GCP_AUTH_PATH
    # auth_session = create_id_token_and_auth_session(GCP_AUTH_PATH, target_audience=
    #                                                "https://www.googleapis.com/auth/cloud-platform")

    try:
        service_account_keyfile_path = os.environ["GOOGLE_APPLICATION_CREDENTIALS"]
        credentials = service_account.Credentials.from_service_account_file(
            service_account_keyfile_path,
            # Generic scope
            scopes=['https://www.googleapis.com/auth/cloud-platform']
        )
        credentials.refresh(Request())
        token = credentials.token
    except Exception as e:
        logger.error("Error while fetching oauth2 token: {}".format(str(e)))
        return None
    return token

def get_current_user_email():
    try:
        current_user = _u.get_current_user()
        return current_user['email']
    except Exception as e:
        logger.error('Failed to fetch current user email with error {}'.format(e))