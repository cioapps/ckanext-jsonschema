

import json
import requests
import ckan.plugins as p
import ckan.plugins.toolkit as toolkit
import ckanext.jsonschema.interfaces as _i
from ckanext.jsonschema.indexers.indexer_utils import create_token, get_current_user_email

import ckanext.jsonschema.constants as _c
import ckanext.jsonschema.tools as _t
from ckanext.jsonschema.indexers.constants import EVENTARC_TARGETING_PIPELINE, EVENTARC_PROJECT_ID, EVENTARC_LOCATION, EVENTARC_MESSAGE_BUS_NAME, EVENTARC_SPEC_VERSION
from ckanext.jsonschema.indexer_controller import ICustomIndexerController, IndexOperationType
from ckanext.jsonschema.indexers.bucket_indexer import package_bucket_indexer, resource_bucket_indexer, view_bucket_indexer

import logging

logger = logging.getLogger(__name__)

#############################################

config = toolkit.config

TYPE_PUBLICATION_RESOURCE = 'geospatial-publication-resource'
FORMAT_PUBLICATION_RESOURCE = 'geospatial-publication'

gs_prefix = 'gs://'
def _extract_publication_resource(data, errors, context):
    #url = data.get('url', None)
    #if not url:
    #    raise Exception("Missing parameter url")

    _body = _t.get_package_body(data)
    # input_vfs = _body.get('input_vfs', '')

    # source_url = _body.get('source_url')

    # if source_url and source_url.startswith(gs_prefix):
    #     source_url = "{}{}".format(input_vfs, source_url[len(gs_prefix):])
    # elif source_url and source_url.startswith(input_vfs):
    #     pass
    # else:
    #     source_url = "{}{}".format(input_vfs, source_url)

    # _body['source_url'] = source_url
    # _t.set_resource_body(data, _body)

    data.update({
        #'url' : url,
        'format': FORMAT_PUBLICATION_RESOURCE,
        'name': "Ingestion-{}".format(_body.get('destination_layername'))
    })

supported_resource_types = {
    TYPE_PUBLICATION_RESOURCE: _extract_publication_resource
}


# Not used at the moment
class JsonschemaEventPublisher(p.SingletonPlugin):
    p.implements(p.IConfigurer)
    # implement the custom indexer interface
    p.implements(_i.IBinder, inherit=True)
    p.implements(ICustomIndexerController, inherit=True)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'ckanext-jsonschema')

    def get_resource_extractor(self, package_type, resource_type, context):

        extractor_for_type = supported_resource_types.get(resource_type)

        if extractor_for_type:
            return extractor_for_type
        else:
            raise KeyError(
                'Extractor not defined for package with type {}'.format(resource_type))

    # def get_input_types(self):
    #     return input_types.keys()

    # def get_supported_types(self):
    #     return supported_types.keys()

    def get_supported_resource_types(self):
        return [TYPE_PUBLICATION_RESOURCE]

    # def get_clonable_resource_types(self):
    #     return clonable_resources_types.keys()

    priority = 10

    def custom_index_package(self, operation, document, package):

        self.publish_indexer_event(package_bucket_indexer, obj=package, obj_type="package", operation=operation)


    def custom_index_resource(self, operation, document, package, resource, is_caller_package_index=False):

        self.publish_indexer_event(resource_bucket_indexer, obj=resource, obj_type="resource", operation=operation, is_caller_package_index=is_caller_package_index)


    def custom_index_view(self, operation, document, package, view):
        self.publish_indexer_event(view_bucket_indexer, obj=view, obj_type="view", operation=operation)


    def publish_indexer_event(self, bucket_indexer, obj=None, obj_type=None, operation=None, is_caller_package_index=False):
        """
        Publishes messages to Eventarc Bus consisting of the Indexes for objects (package, resource, or view) in gcp bucket

        :param organization: String representing the organization name (required if no document is provided).
        :param document: Dictionary containing organization and other metadata (e.g., org_id, res_id, pkg_id) (optional).
        :param obj: The object to index in cloud storage (package, view, or resource).
        :param obj_type: A string indicating the type of object being uploaded ('view', 'resource', 'package') (required).
        :param operation: CRUD operation type (in indexers only create or delete) (optional).
        """
        try:

            # Check if the bucket exists
            bucket_exist = bucket_indexer.bucket_helper.check_bucket_exists()
            if bucket_exist:
                event_data = {
                    # TODO detect correct type
                    "source": "/ckan", # will append
                    "object_type": obj_type,
                    "object_format": obj.get('format'),
                    #"action_type": operation.value,
                    "is_caller_package_index": is_caller_package_index,
                    "item_id": obj.get('id'),
                    "uri": "{}_{}".format(obj.get('id'), obj.get('revision_id')),
                    #"url": "https://storage.googleapis.com/{}/{}/{}.json".format(bucket_indexer.bucket_name, bucket_indexer.bucket_relative_path, obj.get('id')),
                    "obj_payload": json.dumps(obj),
                    "user_email": get_current_user_email()
                }
                publish_event(event_data, operation)

        except Exception as e:
            logger.error(
                "An error occurred while publishing event for {}: {}".format(obj_type, e))

# Specific event publisher logic for jsonschema


def publish_event(event_data, operation):
    project_id = EVENTARC_PROJECT_ID
    location = EVENTARC_LOCATION
    message_bus = EVENTARC_MESSAGE_BUS_NAME
    spec_version = EVENTARC_SPEC_VERSION

    # id + source should be unique for each message
    # https://cloud.google.com/eventarc/advanced/docs/publish-events/publish-events-direct-format
    event_id = event_data['uri']
    event_source = event_data.get('source', '/ckan')

    object_type = event_data.get('object_type')

    if operation == IndexOperationType.CREATE:
        operation_type = "index"
    elif operation == IndexOperationType.DELETE:
        operation_type = "delete"
    else:
        raise Exception("Index Operation Type is not recognized")

    event_type = "{}-{}".format(operation_type, object_type)
    if event_data.get('is_caller_package_index'):
        event_type = "{}-by-package-index".format(event_type)
    event_attributes = {
        "object_format": event_data.get('object_format'),
        "targeting_pipeline": EVENTARC_TARGETING_PIPELINE
    }
    # Construct the URL for the Eventarc API
    base_url = "https://eventarcpublishing.googleapis.com/v1/projects/{}/locations/{}/messageBuses/{}".format(
        project_id, location, message_bus)
    url = "{}:publish".format(base_url)

    # Prepare the payload
    inner_event_data = {
        "specversion": spec_version,
        "type": event_type,
        "source": event_source,
        "id": event_id,
        "data": event_data,
        #"attributes": event_attributes
        "attributes": json.dumps(event_attributes)
    }
    inner_json_string = json.dumps(inner_event_data)
    payload = {
        "jsonMessage": inner_json_string
    }

    # token = create_token("https://www.googleapis.com/auth/cloud-platform")
    token = create_token()
    if not token:
        return

    headers = {
        "Content-Type": "text/plain",
        # "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(token)
    }

    # Make the POST request to publish the event
    response = requests.post(url, headers=headers, data=json.dumps(payload))
    # response = auth_session.post(url, headers=headers, data=json.dumps(payload))

    # Check the response
    if response.status_code == 200:
        logger.info("Event published successfully:{}".format(
            inner_json_string))
    else:
        logger.warning("Failed to publish event: {} with error: {}".format(
            inner_json_string, response.text))


