

import ckan.plugins as p
import ckan.plugins.toolkit as toolkit

import ckanext.jsonschema.bucket_indexer as _bi
import ckanext.jsonschema.constants as _c
from ckanext.jsonschema.indexer_controller import ICustomIndexerController, IndexOperationType

import logging

logger = logging.getLogger(__name__)

#############################################

import json
config = toolkit.config


package_bucket_indexer = _bi.BucketIndexer(_c.BUCKET_NAME, _c.BUCKET_PACKAGES_PATH)
resource_bucket_indexer = _bi.BucketIndexer(_c.BUCKET_NAME, _c.BUCKET_RESOURCES_PATH)
view_bucket_indexer = _bi.BucketIndexer(_c.BUCKET_NAME, _c.BUCKET_VIEWS_PATH)

class JsonschemaBucketIndexer(p.SingletonPlugin):
    p.implements(p.IConfigurer)
    # implement the custom indexer interface
    p.implements(ICustomIndexerController, inherit=True)
                                            

    # IConfigurer
    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'ckanext-jsonschema')

    
    # def get_input_types(self):
    #     return input_types.keys()

    # def get_supported_types(self):
    #     return supported_types.keys()

    # def get_supported_resource_types(self):
    #     return supported_resource_types.keys()

    # def get_clonable_resource_types(self):
    #     return clonable_resources_types.keys()

    priority = 1

    def custom_index_package(self, operation, document, package):
        if operation == IndexOperationType.CREATE:
           self.bucket_index_create(package_bucket_indexer, package, package)
        elif operation == IndexOperationType.DELETE:
            self.bucket_index_delete(package_bucket_indexer, package, package)

    def custom_index_resource(self, operation, document, package, resource, is_caller_package_index=False):
        if operation == IndexOperationType.CREATE:
           self.bucket_index_create(resource_bucket_indexer, package, resource)
        elif operation == IndexOperationType.DELETE:
            self.bucket_index_delete(resource_bucket_indexer, package, resource)

    def custom_index_view(self, operation, document, package, view):
        if operation == IndexOperationType.CREATE:
           self.bucket_index_create(view_bucket_indexer, package, view)
        elif operation == IndexOperationType.DELETE:
            self.bucket_index_delete(view_bucket_indexer, package, view)

    def bucket_index_create(self, bucket_indexer, package, obj):
        # Even if it is an index create operation, if the package is private we need to delete the file on public bucket
        if package.get('private') == False:
            operation = _c.UPLOAD_OPERATION_KEY
        else:
            operation = _c.DELETE_OPERATION_KEY

        try:
            if operation == _c.UPLOAD_OPERATION_KEY: ### Ok or should we change ???
                bucket_indexer.upload_body(
                    obj, obj.get('id'))
            else:
                bucket_indexer.delete_body(obj.get('id'))
        except Exception as e:
            logger.error(str(e))

    def bucket_index_delete(self, bucket_indexer, package, obj):
        try:
            bucket_indexer.delete_body(obj.get('id'))
        except Exception as e:
            logger.error(str(e))
