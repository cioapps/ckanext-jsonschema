import pysolr
import socket

from ckan.lib.search.common import  SearchError
from ckan.logic import  ValidationError
import ckan.authz as authz
from ckan.common import config
import ckan.lib.search.query as ckan_query
import ckan.lib.plugins as lib_plugins
from ckanext.jsonschema.indexer import get_connection
from ckanext.jsonschema.constants import SOLR_DEFAULT_QUERY
from ckan.lib.search import SearchIndexError
import logging

log = logging.getLogger(__name__)

def get_default_search_params(context, params):

    page_size = int(params.get('page_size', 100))
    if page_size > 1000:
        raise ValidationError(
            'Parameter \'page_size\' maximum value is 100, try refining your query parameter')

    user = context.get('user')

    if context.get('ignore_auth') or (user and authz.is_sysadmin(user)):
        labels = None
    else:
        labels = lib_plugins.get_permission_labels(
        ).get_user_dataset_labels(context['auth_user_obj'])
    q = params.get('q')
    if q is None:
        q = SOLR_DEFAULT_QUERY
    fq = []
    if params.get('fq'): fq.append(params.get('fq')) 
    fq.append(" +site_id:\"%s\" " % config['ckan.site_id'])

    if labels is not None:
        fq.append('+permission_labels:(%s)' % ' OR '.join(
            ckan_query.solr_literal(p) for p in labels))
    search_params = dict(params)
    search_params.update({
        "q" : q,
        "fq":  fq
    })

    return search_params


def search(context, core_name, params):
    try:
        search_params = get_default_search_params(
            context, params)
    except Exception as e:
        raise SearchError(str(e))
    conn = get_connection(core_name)
    try:
        return conn.search(**search_params)
    except socket.error as e:
        err = 'Could not connect to SOLR %r: %r' % (conn.url, e)
        log.error(err)
        raise SearchIndexError(err)
    except pysolr.SolrError as e:
        err = 'SOLR %r exception: %r' % (conn.url, e)
        log.error(err)
        raise SearchIndexError(err)