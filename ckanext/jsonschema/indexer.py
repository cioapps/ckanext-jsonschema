import ckan.plugins as plugins
from indexer_controller import ICustomIndexerController
from ckan.lib.search import SearchError
import logging
import socket
import re
import urllib
import pysolr
import simplejson
from os.path import join
import ckanext.jsonschema.tools as _t
import ckanext.jsonschema.constants as _c
import requests
try:
    from urllib.parse import urljoin
except:
    from urlparse import urljoin
from ckan.common import config
from paste.deploy.converters import asbool
# use
# &wt=json for json response writer
# or
# &wt=python
# see https://solr.apache.org/guide/6_6/response-writers.html
from ckan.lib.search.common import SolrSettings, SearchIndexError, solr_datetime_decoder

log = logging.getLogger(__name__)

solr_connections = {}

JSON_PARAMS = {
    'wt': 'json'
}


class Indexer():
    def __init__(self, config_file_path, base_url, core_name, user_name, password):
        self.base_url = base_url
        self.core_name = core_name
        self.config_file_path = config_file_path
        self.user_name = user_name
        self.password = password
        self.init_core()

    def solr_upload(self, dict, defer_commit= False):
        try:
            conn = get_connection(self.core_name, self.base_url, self.user_name, self.password)
            commit = not defer_commit
            if not asbool(config.get('ckan.search.solr_commit', 'true')):
                commit = False
            conn.add(docs=[dict], commit=commit)
        except pysolr.SolrError as e:
            msg = 'Solr returned an error: {0}'.format(
                e[:1000]  # limit huge responses
            )
            raise SearchIndexError(msg)

    def delete_dict(self, query):
        conn = get_connection(self.core_name, self.base_url, self.user_name, self.password)
        try:
            commit = asbool(config.get('ckan.search.solr_commit', 'true'))
            conn.delete(q=query, commit=commit)

        except Exception as e:
            log.exception(e)
            raise SearchIndexError(e)

    # curl -X POST -H 'Content-Type: application/json' 'http://localhost:8983/solr/ckan_views/update?commit=true' -d '{ "delete": {"query":"*:*"} }'


    def init_core(self):

        log.info('Initializing core: {}'.format(self.core_name))

        if self.core_exists():
            log.info('Core exists with name: {}'.format(self.core_name))
            return
        else:
            log.error('THE SOLR CORE NAMED: {} DOES NOT EXIST!'.format(self.core_name))
            # TODO fix the issues on multiple core creation (same configset)
            #self.core_create()
            #self.configure_fields()
            #log.info('Initialized core: {}'.format(self.core_name))
            return

    def core_exists(self):
        response = self.core_status()
        status = response.json().get('status')
        if status:
            core_info = status.get(self.core_name)
            return core_info != {}
        return False

    def core_status(self):

        # TODO 
        # http://localhost:8983/solr/admin/cores?wt=json&action=STATUS&core=jsonschema_views
        # http://localhost:8983/solr/jsonschema_views/schema?wt=json
        #
        # see also https://solr.apache.org/guide/6_6/coreadmin-api.html#CoreAdminAPI-REQUESTSTATUS
        
        query_params = JSON_PARAMS.copy()
        query_params.update(
            {
            'action': 'STATUS',
            'core': self.core_name
            }
        )
        return self.indexer_get_base_admin(query_params)

    def indexer_get_base_admin(self, params):
        url = '{}/admin/cores'.format(_c.SOLR_BASE_URL)
        response = requests.get(url, params)
        if response.status_code == 200:
            response_content = response.json()
            if 'errors' in response_content:
                for error in response_content['errors']:
                    for error_message in error['errorMessages']:
                        log.error('Errors: {}'.format(error_message))
                raise Exception('solr error at:' + url)
            return response
        else:
            raise Exception('solr returned status:' +
                            str(response.status_code) + ' at: ' + url)
    def core_create(self):
        # http://localhost:8983/solr/admin/cores?action=CREATE&name={self.core_name}&instanceDir={self.core_name}
        
        query_params = JSON_PARAMS.copy()
        query_params.update({
            'configSet':'basic_configs',
            'action':'CREATE',
            'name': self.core_name
        })

        log.info('Creating core: {}'.format(self.core_name))
        return self.indexer_get_base_admin(query_params)

    def core_unload(self, purge=False):

        endpoint = 'admin/cores'
        query_params = JSON_PARAMS.copy()
        query_params.update({
            'action':'UNLOAD',
            'core': self.core_name,
            'deleteInstanceDir': 'true' if purge else 'false' 
        })


        if purge:
            log.warn('Unloading core {} and all of its data'.format(self.core_name))
        else:
            log.warn('Unloading core: {}'.format(self.core_name))

        return self.indexer_get_base_admin(query_params)

    def core_reindex(self, jsonschema_type):
        pass

    def core_reindex_all(self):
        # for _type in register.types:
        #     reindex(_type)
        pass

    # Deprecated
    def _get_indexer_representation(self, jsonschema_type, data):
        '''
        '''
        
        plugin = self.get_plugin()
        return plugin.get_indexer_representation(data) # flat result
        
    def update(self, data):

        data = {

        }


        # TODO dict must be flat (defined by an interface method (By jsonschema_type))
        pass

    # TODO define interface IBinder?
    # def configure_from_xml() 
        # one option is to have schema xml as a hardcoded json and to be called in initialization of solr view core
        # the other is to use the solr url and post the xml

    def configure_fields(self):
        
        endpoint = '{}/schema'.format(self.core_name)

        # data = {
        #     "add-field":[
        #         { 
        #             "name":"shelf",
        #             "type":"myNewTxtField",
        #             "stored": True 
        #         },
        #         { 
        #             "name":"location",
        #             "type":"myNewTxtField",
        #             "stored":True 
        #         }
        #     ]
        # }

        query_params = JSON_PARAMS.copy()

        schema_file_type = 'json'
        for operation_name in _c.SOLR_CORE_CREATE_OPS:

            schema_file = '{}.{}'.format(operation_name, schema_file_type)
            with open(join(self.config_file_path,schema_file), "r") as f:
                response = self._indexer_post(endpoint, query_params, f.read())
        return 


        # To add a new field to the schema, follow this example from the Bash prompt:

        # $ curl -X POST -H 'Content-type:application/json' --data-binary '{
        # "add-field":{
        #     "name":"new_example_field",
        #     "type":"text_general",
        #     "stored":true }
        # }' https://..../api/collections/jsonschema_views/schema

        # {
        # "add-field":[
        #     { "name":"shelf",
        #     "type":"myNewTxtField",
        #     "stored":true },
        #     { "name":"location",
        #     "type":"myNewTxtField",
        #     "stored":true }]
        # }

        # NOTE: these configurations can be part of the REGISTRY config??

        # see https://solr.apache.org/guide/6_6/schema-api.html#SchemaAPI-EXAMPLES
        # http://localhost:8983/solr/gettingstarted/schema

    def index(self):
        pass

        # from search/index.py/CkanLibSearch/index_package
        #  # send to solr:
        #     try:
        #         conn = make_connection()
        #         commit = not defer_commit
        #         if not asbool(config.get('ckan.search.solr_commit', 'true')):
        #             commit = False
        #         conn.add(docs=[pkg_dict], commit=commit)
        #     except pysolr.SolrError as e:
        #         msg = 'Solr returned an error: {0}'.format(
        #             e[:1000] # limit huge responses
        #         )
        #         raise SearchIndexError(msg)
        #     except socket.error as e:
        #         err = 'Could not connect to Solr using {0}: {1}'.format(conn.url, str(e))
        #         log.error(err)
        #         raise SearchIndexError(err)


    # def search(self.core_name):
    #     ''' 
    #         - We need a query by id (single result) 
    #         - We need a free text like query in each field
    #     '''
    #     pass

    def _indexer_get(self, endpoint, params={}):
        url = urljoin(self.base_url, endpoint) 
        response = requests.get(url, params)
        if response.status_code == 200:
            try:
                response_content = response.json()
                if 'errors' in response_content:
                    for error in response_content['errors']:
                        for error_message in error['errorMessages']:
                            log.error('Errors: {}'.format(error_message))
                    raise Exception('solr error at:' + url)
            except:
                pass
            return response
        else:
            raise Exception('solr returned status:' + str(response.status_code) + ' at: ' + url)

    def _indexer_post(self, endpoint, headers, payload={}):
        #import json
        #payload = json.dumps(payload)
        url = urljoin(self.base_url, endpoint)
        response = requests.post(url, payload, headers=headers)
        if response.status_code == 200:
            response_content = response.json()
            if 'errors' in response_content:
                for error in response_content['errors']:
                    for error_message in error['errorMessages']:
                        log.error('Errors: {}'.format(error_message))
                raise Exception('solr error at:' + url)

            return response
        else:
            raise Exception('solr returned status:' +
                            str(response.status_code) + ' at: ' + url + '\n' + response.text)


    def delete_package(self, pkg_dict):
        pass

        # conn = make_connection()
        # query = "+%s:%s AND +(id:\"%s\" OR name:\"%s\") AND +site_id:\"%s\"" % \
        #         (TYPE_FIELD, PACKAGE_TYPE, pkg_dict.get('id'), pkg_dict.get('id'), config.get('ckan.site_id'))
        # try:
        #     commit = asbool(config.get('ckan.search.solr_commit', 'true'))
        #     conn.delete(q=query, commit=commit)
        # except Exception as e:
        #     log.exception(e)
        #     raise SearchIndexError(e)


def make_solr_connection(core_name, solr_core_url = None, solr_core_user = None, solr_core_password=None, decode_dates=True):
    if  core_name is None:
        raise Exception('core name must be present !')

    if solr_core_url or solr_core_user or solr_core_password:
        solr_url = solr_core_url
        solr_user = solr_core_user
        solr_password = solr_core_password
    else:
        solr_url, solr_user, solr_password = SolrSettings.get()
        solr_url = '{}/{}'.format(_c.SOLR_BASE_URL, core_name)

    if solr_url and solr_user:
        # Rebuild the URL with the username/password
        protocol = re.search('http(?:s)?://', solr_url).group()
        solr_url = re.sub(protocol, '', solr_url)
        solr_url = "{}{}:{}@{}".format(protocol,
                                       urllib.quote_plus(solr_user),
                                       urllib.quote_plus(solr_password),
                                       solr_url)

    if decode_dates:
        decoder = simplejson.JSONDecoder(object_hook=solr_datetime_decoder)
        return pysolr.Solr(solr_url, decoder=decoder)
    else:
        return pysolr.Solr(solr_url)

def _make_solr_connection(core_name, decode_dates=True):
    if core_name is None:
        raise Exception('core name must be present !')
    solr_url, solr_user, solr_password = SolrSettings.get()
    solr_url = get_default_core_url(solr_url, core_name)

    if solr_url and solr_user:
        # Rebuild the URL with the username/password
        protocol = re.search('http(?:s)?://', solr_url).group()
        solr_url = re.sub(protocol, '', solr_url)
        solr_url = "{}{}:{}@{}".format(protocol,
                                       urllib.quote_plus(solr_user),
                                       urllib.quote_plus(solr_password),
                                       solr_url)

    if decode_dates:
        decoder = simplejson.JSONDecoder(object_hook=solr_datetime_decoder)
        return pysolr.Solr(solr_url, decoder=decoder)
    else:
        return pysolr.Solr(solr_url)


def get_connection(core_name, solr_core_url=None, solr_core_user=None, solr_core_password=None):
    if solr_connections.get(core_name):
        # TODO check connection status  ----->>>> i do not think we could do that
        return solr_connections.get(core_name)
    solr_connections[core_name] = make_solr_connection(
        core_name, solr_core_url, solr_core_user, solr_core_password)
    #solr_connections[core_name] = make_solr_connection(core_name)
    return solr_connections.get(core_name)

def get_default_core_url(solr_url, core_name):
    if _c.SOLR_DEFAULT_CORE_NAME != '':
        return solr_url.replace(_c.SOLR_DEFAULT_CORE_NAME, core_name)
    else:
        return '{}/{}'.config(solr_url, core_name)

custom_indexer_plugins_priority_list = None


def handle_custom_indexing(operation, obj, obj_type, document=None, package=None, is_caller_package_index=False):
    global custom_indexer_plugins_priority_list
    if custom_indexer_plugins_priority_list is None:
        custom_indexer_plugins_priority_list = initialize_custom_indexer_plugins_priority_list()
    for plugin in custom_indexer_plugins_priority_list:
        # if obj_type == "package":
        #     method = plugin.custom_index_package
        # if obj_type == "resource":
        #     method = plugin.custom_index_resource
        # if obj_type == "view":
        #     method = plugin.custom_index_view
        # method(operation,  document, package, obj)
        try:
            if obj_type == "package":
                plugin.custom_index_package(
                    operation=operation,  document=document, package=obj)
            if obj_type == "resource":
                # is_caller_package_index could be added to other object types when necessary
                plugin.custom_index_resource(
                    operation=operation,  document=document, package=package, resource=obj, is_caller_package_index=is_caller_package_index)
            if obj_type == "view":
                plugin.custom_index_view(
                    operation=operation,  document=document, package=package, view=obj)
        except Exception as e:
            log.error(str(e))

def initialize_custom_indexer_plugins_priority_list():
    # A loader for plugins implementing ICustomIndexerController interface
    # Runs only once
    custom_indexer_plugins = []
    for plugin in plugins.PluginImplementations(ICustomIndexerController):
        # for i in range(len(custom_indexer_plugins)):
        custom_indexer_plugins.append(plugin)
    custom_indexer_plugins = sorted(
        custom_indexer_plugins, key=lambda plugin: plugin.priority)
    return custom_indexer_plugins
