import ckan.plugins.toolkit as toolkit
import logging

logger = logging.getLogger(__name__)

# # Deprecated, graph logic
# def push_all_orgs_with_users_action(context, data_dict):
#     """
#     Fetches all organizations and their users, and pushes them to the graph.

#     This action is restricted to sysadmin users only.
#     """

#     toolkit.check_access('sysadmin', context)
#     try:
#         _gi.push_all_orgs_with_users()
#         return {'success': True, 'message': 'All organizations and users have been pushed to the graph.'}
#     except Exception as e:
#         logger.error("Error pushing organizations to the graph: {}".format(e))
#         raise toolkit.ValidationError("Error occurred: {}".format(e))

# # Deprecated, graph logic
# def push_missing_orgs_with_users_action(context, data_dict):
#     """
#     Fetches missing organizations and their users, and pushes them to the graph.

#     This action is restricted to sysadmin users only.
#     """
#     toolkit.check_access('sysadmin', context)
#     try:
#         _gi.push_missing_orgs_with_users()
#         return {'success': True, 'message': 'Missing organizations and users have been pushed to the graph.'}
#     except Exception as e:
#         logger.error("Error pushing missing organizations to the graph: {}".format(e))
#         raise toolkit.ValidationError("Error occurred: {}".format(e))
