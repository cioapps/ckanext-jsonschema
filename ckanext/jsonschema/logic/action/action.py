# encoding: utf-8
from ckan.lib.search import SearchIndexError
import ckan.model.domain_object as domain_object
from ckan.plugins import toolkit
import ckanext.jsonschema.validators as _v
import ckanext.jsonschema.tools as _t
import ckanext.jsonschema.indexer as _i
import ckan.model as model
import ckan.lib.navl.dictization_functions as df
import logging
import json

import ckan.logic as logic
import ckan.plugins as plugins
from ckan.common import _

log = logging.getLogger(__name__)

# Define some shortcuts
# Ensure they are module-private so that they don't get loaded as available
# actions in the action API.
NotFound = logic.NotFound
NotAuthorized = logic.NotAuthorized
ValidationError = logic.ValidationError


StopOnError = df.StopOnError

# @plugins.toolkit.chained_action
# def organization_create(next_auth, context, data_dict):
#     # do the actual stuff here
#     next = next_auth(context, data_dict)
#     copy_data_dict = json.loads(json.dumps(data_dict))
#     copy_data_dict.update({'item_id': copy_data_dict.get('id')})
#     _gi.generic_neo4j_api_call("nodes", [copy_data_dict])
#     return next

@plugins.toolkit.chained_action
def package_delete(next_auth, context, data_dict):
    package_id = data_dict.get('id')
    index_delete_subindexes(context, package_id)
    index_delete_package(package_id)
    return next_auth(context, data_dict)

@plugins.toolkit.chained_action
def resource_create(next_auth, context, data_dict):
    # We would like to do this, but...
    # Resources are different from packages. If we do this, jsonschema fields are flattened and clash with validators
    # The input field [(u'resources', 4, u'jsonschema_body', 0, u'type'), ...] was not expected.

    #for view in data_dict['views']:
    next = validate_resource(next_auth, context, data_dict)
    return next

@plugins.toolkit.chained_action
def resource_update(next_auth, context, data_dict):
    ### we are not firing a view indexing event as the package before_index is called by ckan triggering that pattern ###
    next =  validate_resource(next_auth, context, data_dict)
    return next

@plugins.toolkit.chained_action
def resource_delete(next_auth, context, data_dict):
    next =  next_auth(context, data_dict)
    resource_id = data_dict.get('id')
    index_delete_views(context, resource_id)
    return next

def validate_resource(next_auth, context, data_dict):

    errors = {}
    key = ''

    _type = _t.get_resource_type(data_dict)

    if not _type:
        # not a jsonschema resource, skip validation and extraction
        return next_auth(context, data_dict)

    body = _t.as_dict(_t.get_resource_body(data_dict))
    opt = _t.as_dict(_t.get_resource_opt(data_dict))

    package = toolkit.get_action('package_show')(
        context, {'id': data_dict.get('package_id')})
    package_type = _t.get_package_type(package)

    ######################### TODO #########################

    if opt and opt.get('validation') == False:
        return
    ######################### #### #########################

    try:

        _v.item_validation(_type, body, opt, key, errors, context)

        _v.resource_extractor(data_dict, package_type, errors, context)

    except df.StopOnError:
        raise ValidationError(df.unflatten(errors))

    return next_auth(context, data_dict)


@plugins.toolkit.chained_action
def resource_view_update(next_auth, context, data_dict):
    next = next_auth(context, data_dict)
    notify_index_view(context, next, domain_object.DomainObjectOperation.changed)
    return next


@plugins.toolkit.chained_action
def resource_view_create(next_auth, context, data_dict):
    next = next_auth(context, data_dict)
    notify_index_view(context, next, domain_object.DomainObjectOperation.new)
    return next


@plugins.toolkit.chained_action
def resource_view_delete(next_auth, context, data_dict):
    notify_index_view(context, data_dict,domain_object.DomainObjectOperation.deleted)
    return next_auth(context, data_dict)

def index_package(data_dict):
    package_id = data_dict.get('package_id')

    if not package_id:
        if 'id' in data_dict:  # this is the view id
            try:
                resource_view = toolkit.get_action('resource_view_show')(
                    None, {u'id': data_dict.get('id')})
                package_id = resource_view.get('package_id')
            except:
                log.error(
                    'Unable to locate the resource for view id: {}'.data_dict.get('id'))
                pass
        elif 'resource_id' in data_dict:
            try:
                resource = toolkit.get_action('resource_show')(
                    None, {u'id': data_dict.get('resource_id')})
                package_id = resource.get('package_id')
            except:
                log.error('Unable to locate the package for resource id: {}'.data_dict.get(
                    'resource_id'))
                pass

    if not package_id:
        log.error(
            'Aborting: Unable to reindex the package for view id: {}'.data_dict.get('id'))
        return

    package = model.Package.get(package_id)


    trigger_notify(package, domain_object.DomainObjectOperation.changed)

def index_resource(context, resource, operation):
    if context.get('prevent_notify'):
        return

    # view_id = view.id
    resource_model = context.get('resource')
    if not resource_model:
        resource_model = model.Resource.get(resource.get('id'))
    else:
        pass
    
    trigger_notify(resource_model, operation)


def notify_index_view(context, view, operation):
    if context.get('prevent_notify'):
        return
    # view_id = view.id
    view_model = context.get('resource_view')
    if not view_model:
        view_model = model.ResourceView.get(view.get('id'))
    else:
        pass
        # TODO merge view model with view

    #if not view_model.get('package_id'):
        # package_id may not be there
    #    view_model.package_id = view.get('package_id')

    trigger_notify(view_model, operation)


def trigger_notify(model, operation):
    # code from ckan/model/modification
    for observer in plugins.PluginImplementations(plugins.IDomainObjectModification):
        try:
            observer.notify(
                model, operation)
        except SearchIndexError as search_error:
            log.exception(search_error)  # i am not so sure about this
            raise
        except Exception as ex:
            log.exception(ex)
            # Don't reraise other exceptions since they are generally of
            # secondary importance so shouldn't disrupt the commit.


def index_delete_subindexes(context, package_id):
    pkg = toolkit.get_action('package_show')(None, {'id': package_id})
    if not pkg:
        raise Exception('Unable to find package with id: {}'.format(package_id))

    pkg = _t.dictize_pkg(pkg)
    resources = pkg.get('resources', [])
    for resource in resources:
        index_resource(context, resource,
                        domain_object.DomainObjectOperation.deleted)
        index_delete_views(context, resource.get('id'))


def index_delete_views(context, resource_id):
    
    resource_views = toolkit.get_action('resource_view_list')({}, {
        'id': resource_id})
    for view in resource_views:
        notify_index_view(
            context, view, domain_object.DomainObjectOperation.deleted)


def index_delete_package(package_id):  # TODO could be refactored
    try:

        package = logic.get_action('package_show')({'model': model},{'id': package_id})
        from ckanext.jsonschema.indexer_controller import IndexOperationType

        _i.handle_custom_indexing(operation=IndexOperationType.DELETE, obj=package, obj_type="package")
    except Exception as e:
        log.error(str(e))
    # try:
    #     from ckanext.jsonschema.plugin import bucket_indexer
    #     bucket_indexer.delete_body(package_id)
    # except Exception as e:
    #     log.error(str(e))