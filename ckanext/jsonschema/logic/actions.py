import json
from datetime import date, datetime
from ckan.common import config
import ckan.model as model
import ckan.lib.navl.dictization_functions as df
import ckan.plugins.toolkit as toolkit
import ckanext.jsonschema.configuration as configuration
import ckanext.jsonschema.constants as _c
# import ckanext.jsonschema.logic.get as _g
import ckanext.jsonschema.tools as _t
import ckanext.jsonschema.view_tools as _vt
import ckanext.jsonschema.utils as _u
import ckanext.jsonschema.solr_tools as _st

# import ckanext.jsonschema.validators as _v
from ckan.logic import NotFound, ValidationError, side_effect_free
from ckan.plugins.core import PluginNotFoundException
from ckan.lib.search import index_for

_ = toolkit._
h = toolkit.h

import logging

log = logging.getLogger(__name__)

import ckan.logic as logic

_check_access = logic.check_access

#view_search_index = index_for(model.ResourceView)  # ViewSearchIndex()
#resource_search_index = index_for(model.Resource)  # ResourceSearchIndex()

def importer(context, data_dict):
    if not data_dict:
        error_msg = 'No dict provided'
        h.flash_error(error_msg,allow_html=True)
        return

    url = data_dict.get('url')
    if not url:
        h.flash_error(_('No url provided'), allow_html=True)
        return

    license_id = data_dict.get('license_id')
    if not license_id:
        h.flash_error(_('License is mandatory'), allow_html=True)
        return


    _check_access('package_create', context, data_dict)
    
    try:
        import requests
        response = requests.get(url, stream=True)
        if response.status_code != 200:
            raise Exception('Unable to fetch data, response status is {}'.format(response.status_code))
    except Exception as e:
        message = str(e)
        log.error(message)
        # e.error_summary = json.dumps(message)
        raise ValidationError(message)

    # body is supposed to be json, if true a  1:1 conversion is provided
    from_xml = data_dict.get('from_xml', 'false').lower() == "true"
    #from_xml = asbool(data_dict.get('from_xml', False))
    try:
        if from_xml:
            body = _u.xml_to_json(response.text)
        else:
            body = response.json() #TODO as json check 
    except Exception as e:
        message = str(e)
        log.error(message)
        # e.error_summary = json.dumps(message)
        raise ValidationError(message)



    # CREATE EXTRAS
    _type = data_dict.get(_c.SCHEMA_TYPE_KEY)
    
    opt = dict(_c.SCHEMA_OPT)
    opt.update({
        'imported' : True,
        'source_format':'xml' if from_xml else 'json',
        'source_url': url,
        'imported_on': str(datetime.now())
    })


    # IMPORT - PREPROCESSING -
    import_context = {}

    package_dict = {
        # IMPORTER_TYPE = 'iso19139'old
        'type': _type,
        'owner_org': data_dict.get('owner_org'),
        'license_id': data_dict.get('license_id'),
        _c.SCHEMA_BODY_KEY: _t.as_dict(body),
        _c.SCHEMA_TYPE_KEY : _type,
        _c.SCHEMA_OPT_KEY : opt,
    }

    errors = []

    try:
        plugin = configuration.get_plugin(_type)
    except PluginNotFoundException as e:
        return { "success": False, "msg": str(e)}

    extractor = plugin.get_input_extractor(_type, package_dict, import_context) 
    extractor(package_dict, errors, import_context)   
    # TODO ######################
    opt['validation'] = False  
    #_t.update_extras_from_context(package_dict, import_context)

    #TODO resources store back to the package_dict
    try:
        # # TODO has deep impact/implications over resources
        is_package_update = data_dict.get('package_update', 'false').lower() == "true"
        #is_package_update = asbool(data_dict.get('package_update', False))
        if is_package_update:
            errors=[]
            #if _type in plugin.supported_dataset_types(opt, _c.SCHEMA_VERSION):
            id = plugin.extract_id(package_dict, errors, context)
            if id:
                existing_pkg = logic.get_action('package_show')(context, {'id': id})
                if existing_pkg:# and _t.get_package_type(existing_pkg) in [_type, 'iso']:
                    updated_existing_pkg = plugin.match_existing_resources(
                        package_dict, existing_pkg, errors, context)

                    existing_opt = _t.get_package_opt(updated_existing_pkg)
                    existing_opt['validation'] = False
                    updated_existing_pkg[_c.SCHEMA_OPT_KEY] = _t.as_dict(existing_opt)

                    return toolkit.get_action('package_update')(context, updated_existing_pkg)

            raise Exception('no support provided for this operation/format')
        else:
            return toolkit.get_action('package_create')(context, package_dict)
    except Exception as e:
        message = str(e)
        log.error(message)
        # e.error_summary = json.dumps(message)
        raise ValidationError(message)
    
    # next_action(context,data_dict)    

def validate_metadata(context, data_dict):

    id = data_dict.get('id')

    # TODO check permissions: package_show?

    package = _t.get(id)

    if package is None:
        raise NotFound("No package found with the specified uuid")

    body = _t.get_package_body(package)
    
    # We need the jsonschema type, so we cannot use _t.get_package_type which would also return datasets
    # _type = _t.get_package_type(package)
    _type = _t._extract_from_package(package, _c.SCHEMA_TYPE_KEY)

    if not _type:
        raise ValidationError("The jsonschema validation is only available on jsonschema metadata")

    errors = {}
    is_error = _t.draft_validation(_type, body, errors)
    
    if is_error:
        raise ValidationError(df.unflatten(errors))


def clone_metadata(context, data_dict):

    source_pkg = _t.get(data_dict.get('id'))
    _type = _t.get_package_type(source_pkg)

    try:
        plugin = configuration.get_plugin(_type)
    except PluginNotFoundException as e:
        return { "success": False, "msg": str(e)}


    package_dict = {
        'resources': [],
        'type': _type,
        'owner_org': data_dict.get('owner_org')
    }
    
    _check_access('package_create', context, package_dict)

    context.update({
        'prevent_notify': True
    })
    clone_context = {}
    # clone_context = {
    #     'prevent_notify': True
    # }

    errors = []

    try:

        cloner = plugin.get_package_cloner(_type)

        if not cloner:
            message = 'No cloner configured for package type {}. Skipping'.format(_type)
            log.info(message)
            return { "success": False, "msg": message}

        cloner(source_pkg, package_dict, errors, clone_context)

        # defaults
        package_dict['private'] = True
        package_dict['title'] = 'Cloned {} {}'.format(source_pkg.get('title',''), datetime.now().isoformat())

        new_pkg_dict = toolkit.get_action('package_create')(context, package_dict)

        for _resource in source_pkg.get('resources'):

            resource = dict(_resource)

            try:
                del resource['id']
                del resource['package_id']

                if 'revision_id' in resource:
                    del resource['revision_id']

                resource['package_id'] = new_pkg_dict['id']
                
                resource_clone_context = {}
                resource_type = _t.get_resource_type(resource)
                
                # default to dataset resource
                if not resource_type:
                    import ckanext.jsonschema.dataset.constants as _dataset_constants
                    resource_type = _dataset_constants.TYPE_DATASET_RESOURCE

                plugin = configuration.get_plugin(_type, resource_type)
                cloner = plugin.get_resource_cloner(_type, resource_type)
                if not cloner:
                    log.info('No cloner configured for resource type {} on package type {}. Skipping'.format(resource_type, _type))
                    continue
                
                resource = cloner(resource, errors, resource_clone_context)
                # if resource:
                #     # attach to package_dict
                #     package_dict['resources'].append(resource)

                resource = toolkit.get_action('resource_create')(context, resource)
                
                # now let's clone the views
                views = toolkit.get_action('resource_view_list')(None,{'id': _resource['id']})
                for view in views:
                    view_dict_clone = {
                        'view_type': view['view_type'],
                        'title': view['title'],
                        'resource_id': resource['id']
                    }

                    # TODO how to clone other custom views?

                    # this cloner will take care of normal views
                    # AND jsonschema views
                    jsonschema_type = view.get('jsonschema_type')

                    if jsonschema_type:
                        view_dict_clone.update({
                            _c.SCHEMA_BODY_KEY: view.get('jsonschema_body'),
                            _c.SCHEMA_TYPE_KEY: jsonschema_type,
                            _c.SCHEMA_OPT_KEY: view.get('jsonschema_opt')
                        })

                    try:
                        vv = toolkit.get_action('resource_view_create')(context, view_dict_clone)
                    except Exception as e:
                        log.error(str(e))
            
            except PluginNotFoundException: #TODO remove, should raise error
                log.error('Unable to find a plugin implementation for resource type {}'.format(resource_type))
                raise

        
        import ckanext.jsonschema.logic.action.action as action
        action.index_package({'package_id': new_pkg_dict.get('id')})

        return new_pkg_dict
        
    except Exception as e:
        import traceback
        traceback.print_exc()

        message = str(e)
        log.error(message)
        raise ValidationError(message)


@side_effect_free
def view_show(context, data_dict):
    view_id = data_dict.get('view_id')
    #resolve = data_dict.get('resolve', False)

    _check_access('resource_view_show', context, {'id': view_id})

    query = 'id:{}'.format(view_id)
    
    # commented out, not properly supported by solr 3.6
    # fl = 'view_*,indexed_ts'
    query_params = {'q' : query}
    try:
        solr_results = _st.search(
            context, _c.SOLR_VIEWS_CORE_NAME, query_params)
        result_documents = solr_results.docs
        if len(result_documents) == 0:
            raise NotFound('Unable to find view: {}'.format(view_id))
        view_document = _t.dictize_pkg(result_documents[0])
    except Exception as e:
        log.exception(e)
        raise ValidationError(str(e))
    content = {
        'indexed_ts': view_document.get('indexed_ts').isoformat(),
        'package_id': view_document.get('pkg_id'),
        'package_capacity': view_document.get('capacity'),
        'resource_id': view_document.get('res_id'),
        'view_id': view_document.get('id'),
        'view_type': view_document.get('type'),
        _c.SCHEMA_TYPE_KEY: _vt.get_view_type(view_document),
        _c.SCHEMA_BODY_KEY: _vt.get_view_body(view_document),
        _c.SCHEMA_OPT_KEY: _vt.get_view_opt(view_document)
    }

    return content


@side_effect_free
def view_list(context, data_dict):

    package_id = data_dict.get('package_id')
    if not package_id:
        raise ValidationError('Parameter \'package_id\' is mandatory')

    try:
        query = 'capacity:public'
        fq = 'pkg_id:{} OR pkg_name:{}'.format(package_id, package_id)
        solr_results = _st.search(context, _c.SOLR_VIEWS_CORE_NAME,
                            params={'q': query, 'fq': fq})
        del solr_results.raw_response['responseHeader']
        dump = json.dumps(solr_results.raw_response, default=_u.default)
        return _t.dictize_pkg(dump)

    except Exception as e:
        raise ValidationError(str(e))


def get_view_count(resources=[]):
    count = 0
    for resource in resources:
        if resource['views']:
            count += len(resource['views'])


@side_effect_free
def search_views(context, params):
    try:
        solr_results = _st.search(context, _c.SOLR_VIEWS_CORE_NAME,params)
        del solr_results.raw_response['responseHeader']
        dump = json.dumps(solr_results.raw_response, default=_u.default)
        return _t.dictize_pkg(dump)
    except Exception as e:
        log.exception(e)
        raise ValidationError(str(e))

@side_effect_free
def search_resources(context, params):

    try:
        solr_results = _st.search(context, _c.SOLR_RESOURCES_CORE_NAME, params)
        del solr_results.raw_response['responseHeader']
        dump = json.dumps(solr_results.raw_response, default=_u.default)
        return _t.dictize_pkg(dump)
    except Exception as e:
        log.exception(e)
        raise ValidationError(str(e))

@side_effect_free
def search_packages(context, params):

    try:
        solr_results = _st.search(context, _c.SOLR_DEFAULT_CORE_NAME, params)
        del solr_results.raw_response['responseHeader']
        dump = json.dumps(solr_results.raw_response, default=_u.default)
        return _t.dictize_pkg(dump)

    except Exception as e:
        log.exception(e)
        raise ValidationError(str(e))

# # Deprecated
# @side_effect_free
# def get_all_parents(context, params):
#     if not _c.GRAPH_INDEXER_ENABLED:
#         return {'success': False, 'msg': 'This functionality does not work when the graph indexer is disabled'}
#     view_id = params.get('view_id')
#     all_parents = _gi.generic_neo4j_api_get(
#         'relationships/get_all_parents', params='child_id={}'.format(view_id))
#     parent_links = []
#     for parent_dict in all_parents:
#         parent_view_id = parent_dict['parent'].get('item_id')
#         view = view_show(context, {'view_id': parent_view_id})
#         if not view:
#             raise Exception(
#                 'View with id {} could not be fetched'.format(parent_view_id))
#         ckan_site_url = config.get('ckan.site_url') + config.get('ckan.root_path')
#         parent_link = '{}dataset/{}/resource/{}'.format(ckan_site_url, view.get('package_id'), view.get('resource_id'))
#         parent_links.append(parent_link)
#     return parent_links