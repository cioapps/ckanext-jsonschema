# from sqlalchemy.sql.expression import true
from ckan.plugins import plugin_loaded
import ckan.plugins.toolkit as toolkit
#from ckan.lib.search.common import SolrSettings
config = toolkit.config
import json
import os
path = os.path
PATH_ROOT=path.realpath(path.join(path.dirname(__file__),'..'))
##############################

# (Internal)
# the name of the plugin
TYPE='jsonschema'

# Schema keys will be part of the extras keys
SCHEMA_OPT_KEY='jsonschema_opt'
SCHEMA_OPT={} # TODO MAKE DEFAULT CONFIG CONFIGURABLE....
SCHEMA_BODY_KEY='jsonschema_body'
# to mark a metadata as 
# ckanext-jsonschema managed package

# TODO schema Mapping
SCHEMA_TYPE_KEY='jsonschema_type'


# (Internal)
##############################
# (Internal)
#  Will contain the schema and template defined with the type-mapping
JSON_SCHEMA_KEY = 'schema'
JSON_TEMPLATE_KEY = 'template'
JSON_OPT_SCHEMA_KEY = 'opt_schema'
JSON_OPT_TEMPLATE_KEY = 'opt_template'
JSON_REGISTRY_KEY = 'registry'

JSONSCHEMA_CONFIG = {}
JS_MODULE_KEY = 'module'
JSON_CATALOG = {
   JSON_SCHEMA_KEY: {},
   JSON_TEMPLATE_KEY: {},
   JS_MODULE_KEY: {},
   JSON_REGISTRY_KEY: {}
}
JSON_CATALOG_INITIALIZED = False
#############################

# (Optional)
# fields to do not interpolate with jinja2 (f.e. they are a template of other type)
# FIELDS_TO_SKIP = config.get('ckanext.jsonschema.skip.fields', ['featureInfoTemplate'])

# (Optional)
# SERVER LOCAL PATH FOLDER WHERE JSON-SCHEMA are located.
PATH_SCHEMA=path.realpath(config.get('ckanext.jsonschema.path.schema', path.join(PATH_ROOT,'schema')))
PATH_CORE_SCHEMA='core'
REST_SCHEMA_PATH='/{}/schema'.format(TYPE)
REST_SCHEMA_FILE_PATH='/{}/schema_file'.format(TYPE)


# (Optional)
# Used as jinja template to initialize the items values, it's name is by convention the type
# same type may also be located under mapping
PATH_TEMPLATE=path.realpath(config.get('ckanext.jsonschema.path.template', path.join(PATH_ROOT,'template')))
REST_TEMPLATE_PATH='/{}/template'.format(TYPE)

#REST_GET_BODY = TODO
#REST_GET_BODY_PATH='/{}/body'.format(TYPE)


# (Optional)
# Used as options
#PATH_OPT=path.realpath(config.get('ckanext.jsonschema.path.opt', path.join(PATH_ROOT,'opt')))
#REST_OPT_PATH='/{}/opt'.format(TYPE)

# (Optional)
# Used as json-editor modules to initialize the UI with js extension points
PATH_MODULE=path.realpath(config.get('ckanext.jsonschema.path.module', path.join(PATH_ROOT,'module')))
REST_MODULE_FILE_PATH='/{}/module'.format(TYPE)

PATH_CONFIG=path.realpath(config.get('ckanext.jsonschema.path.config', path.join(PATH_ROOT,'config')))


# ---- REGISTRY ---- #

FILENAME_REGISTRY='registry.json'

# VIEWS
RESOURCE_FORMATS = 'resource_formats'
WILDCARD_FORMAT = 'available_for_all_resource_formats'
RESOURCE_JSONSCHEMA_TYPE = 'resource_jsonschema_type'
WILDCARD_JSONSCHEMA_TYPE = 'available_for_all_resource_jsonschema_types'
VIEW_JSONSCHEMA_TYPE = 'view_jsonschema_type'
DEFAULT_VIEW = 'default_view'

# BUCKET INDEXING
BUCKET_NAME = config.get(
    'ckanext.jsonschema.bucket_name')
BUCKET_RELATIVE_PATH = config.get(
    'ckanext.jsonschema.bucket_relative_path', '')
BUCKET_VIEWS_PATH = '{}/views'.format(
    BUCKET_RELATIVE_PATH) if BUCKET_RELATIVE_PATH else 'views'
BUCKET_RESOURCES_PATH = '{}/resources'.format(
    BUCKET_RELATIVE_PATH) if BUCKET_RELATIVE_PATH else 'resources'
BUCKET_PACKAGES_PATH = '{}/packages'.format(
    BUCKET_RELATIVE_PATH) if BUCKET_RELATIVE_PATH else 'packages'
GCP_AUTH_PATH = config.get(
    'ckanext.jsonschema.gcp_auth_path', '')
UPLOAD_OPERATION_KEY = 'upload'
DELETE_OPERATION_KEY = 'delete'

# Indexer configs
BUCKET_INDEXER_ENABLED = config.get('ckanext.jsonschema.bucket_indexer_enabled', '').lower() == "true"
TITILER_BASE_URL = config.get('ckanext.jsonschema.titiler_base_url', '')

# SOLR
SKIP_INDEXING = 'skip_indexing'
SOLR_DEFAULT_CORE_NAME = config.get('ckanext.jsonschema.solr.default.core_name', '')
SOLR_BASE_URL = config.get('solr_url', 'http://localhost:8983/solr')
if SOLR_DEFAULT_CORE_NAME != '':
    SOLR_BASE_URL = SOLR_BASE_URL.replace('/{}'.format(SOLR_DEFAULT_CORE_NAME), '')
SOLR_RESOURCES_CORE_NAME = 'ckan_resources'
SOLR_VIEWS_CORE_NAME = 'ckan_views'
SOLR_RESOURCES_USERNAME = config.get(
    'ckanext.jsonschema.solr.ckan_resources.user_name', config.get('ckan_user', ''))
SOLR_RESOURCES_PASSWORD = config.get(
    'ckanext.jsonschema.solr.ckan_resources.user_password',config.get('ckan_password', ''))
SOLR_RESOURCES_BASE_URL = config.get(
    'ckanext.jsonschema.solr.ckan_resources.base_url', '{}/{}'.format(SOLR_BASE_URL, SOLR_RESOURCES_CORE_NAME))
SOLR_RESOURCES_CONFIG_PATH = path.realpath(config.get(
    'ckanext.jsonschema.solr.ckan_resources.config', path.join(PATH_CONFIG, 'solr', SOLR_RESOURCES_CORE_NAME)))

SOLR_VIEWS_USERNAME = config.get(
    'ckanext.jsonschema.solr.ckan_views.user_name', config.get('ckan_user', ''))
SOLR_VIEWS_PASSWORD = config.get(
    'ckanext.jsonschema.solr.ckan_views.user_password', config.get('ckan_password', ''))
SOLR_VIEWS_BASE_URL = config.get(
    'ckanext.jsonschema.solr.ckan_views.base_url', '{}/{}'.format(SOLR_BASE_URL, SOLR_VIEWS_CORE_NAME))
SOLR_VIEWS_CONFIG_PATH = path.realpath(config.get(
    'ckanext.jsonschema.solr.ckan_views.config', path.join(PATH_CONFIG,'solr', SOLR_VIEWS_CORE_NAME)))

SOLR_CORE_CREATE_OPS = [
    'add_field_types',
    'add_fields',
    'add_copy_fields'
]

SOLR_DEFAULT_QUERY = '*:*'
# JINJA CONFIGURATION
SUPPORTED_CKAN_FIELDS = 'supported_ckan_fields'
SUPPORTED_JSONSCHEMA_FIELDS = 'supported_jsonschema_fields'
WILDCARD_CKAN_FIELDS = 'supported_all_ckan_fields'
WILDCARD_JSONSCHEMA_FIELDS = 'supported_all_jsonschema_fields'


# ---- END REGISTRY ---- #

DEFAULT_LICENSE=config.get('ckanext.jsonschema.license.default', 'notspecified')

try:
    if plugin_loaded("cloudstorage"):
        CLOUDSTORAGE_PLUGIN_ENABLED = True
    else:
        CLOUDSTORAGE_PLUGIN_ENABLED = False
except Exception:
    CLOUDSTORAGE_PLUGIN_ENABLED = False
