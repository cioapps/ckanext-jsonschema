import logging
import json
import ckanext.jsonschema.constants as _c
import ckanext.jsonschema.bucket_helper as _bh
import ckanext.jsonschema.view_tools as _vt


logger = logging.getLogger(__name__)

class BucketIndexer():
    def __init__(self, bucket_name, bucket_relative_path):
        self.bucket_name = bucket_name
        self.bucket_relative_path = bucket_relative_path
        self.bucket_indexer_disable = not _c.BUCKET_INDEXER_ENABLED
        try:
            self.bucket_helper = _bh.BucketHelper(
                self.bucket_name, self.bucket_relative_path)
        except Exception as e:
            logger.error(str(e))
            self.bucket_indexer_disable = True

    def upload_body(self, data_dict, id, metadata=None):
        if self.bucket_indexer_disable:
            return
        file_name = "{}.json".format(id)
        self.bucket_helper.upload_bucket_file(json.dumps(data_dict),file_name, metadata)

    def delete_body(self, id):
        if self.bucket_indexer_disable:
            return
        file_name = "{}.json".format(id)
        self.bucket_helper.delete_bucket_file(file_name)

