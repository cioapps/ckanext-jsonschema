
import json
import logging
import socket
import hashlib
import datetime
from ckan.common import config
import ckanext.jsonschema.tools as _t
import ckanext.jsonschema.constants as _c
import ckanext.jsonschema.indexer as _i
import ckanext.jsonschema.bucket_indexer as _bi
import ckanext.jsonschema.view_tools as _vt
import ckanext.jsonschema.utils as utils
import ckanext.jsonschema.configuration as configuration
from ckanext.jsonschema.indexer_controller import IndexOperationType

import ckan.model as model
from ckan.lib.search import SearchIndexError
from ckan.lib.search.index import SearchIndex
from ckan.lib.search.__init__ import _INDICES

indexer = _i.Indexer(_c.SOLR_RESOURCES_CONFIG_PATH, _c.SOLR_RESOURCES_BASE_URL,
                          _c.SOLR_RESOURCES_CORE_NAME, _c.SOLR_RESOURCES_USERNAME, _c.SOLR_RESOURCES_PASSWORD)

# bucket_indexer = _bi.BucketIndexer(
#     _c.BUCKET_NAME, _c.BUCKET_RESOURCES_PATH)

log = logging.getLogger(__name__)

class ResourceSearchIndex(SearchIndex):
    #implements(IJsonschemaView, inherit=True)
    #implements(IResourceView, inherit=True)
    # check if the ckan_view core exists, 

    # SearchIndex
    def __init__(self, *args, **kwargs):
        pass

    def insert_dict(self, data):
        if data is None:
            return
        resource = self.model_to_dict(data)
        if self.skip_index(resource):
            return
        return self.index_resource(resource)
    
    def remove_dict(self, resource):
        if isinstance(resource, model.Resource):
            resource = self.model_to_dict(resource)
        if self.skip_index(resource):
            return

        log.info('Deleting index for resource with id {}'.format(
            resource.get('id')))
        self.delete_resource(resource)

    def update_dict(self, resource, defer_commit=False):
        if resource is None:
            return

        if resource.state != 'active':
            log.info('The resource with id:' + str(resource.id) +
                     ' has already been deleted. Indexing is skipped')
            self.remove_dict(resource)
            return
        resource = self.model_to_dict(resource)
        if self.skip_index(resource):
            return
        self.index_resource(resource, defer_commit)

    def index_resource(self, resource, defer_commit=False, is_caller_package_index=False):
        if self.skip_index(resource):
            return

        resource_jsonschema_type = _t.get_resource_type(resource)

        package_id = resource.get('package_id')
        resource_id = resource.get('id')

        model = _vt.get_model(package_id, resource_id)

        organization = model['organization']
        package = model['package']
        resource = model['resource']

        log.info('Indexing resource: {}'.format(resource_id))
        try:
            document = _before_index_resource(organization, package, resource)
            if resource_jsonschema_type:
                resource_plugin = configuration.get_plugin(resource_jsonschema_type)
                document = resource_plugin.before_index_resource(document, resource)
            else:
                # do not index jsonschema fields if regular type
                log.info('Jsonschema Indexer may not take care of a not jsonschema resource:\
                    skipping jsonschema fields for resource {}'.format(resource.get('id')))

            #if _c.CLOUDSTORAGE_PLUGIN_ENABLED:
            #    utils.bucket_index_cloudstorage(document=document, package=package, obj=resource, obj_type="resource")
            _i.handle_custom_indexing(operation=IndexOperationType.CREATE, obj=resource, obj_type="resource", document=document,
                                      package=package, is_caller_package_index=is_caller_package_index)

            indexer.solr_upload(document)

        except Exception as e:
            log.error(str(e))

    # def clear(self):# TODO we need to implement clear_index by type
        # clear_index()

    def get_all_entity_ids(self):
        raise NotImplemented

    def commit(self):
        try:
            conn = indexer.get_connection()
            conn.commit(waitSearcher=False)
        except Exception as e:
            log.exception(e)
            raise SearchIndexError(e)

    def model_to_dict(self, resource_model):
        #resource_dict = logic.get_action('resource_show')({'model': model, 'ignore_auth': True, 'validate': False,'use_cache': False},{'id': resource_model.id})        
        resource_dict = resource_model.as_dict()
        return resource_dict

    def delete_resource(self, resource):

        query = None
        if resource.get('id'):
            query = "+id:\"%s\"" % (resource.get('id'))

        #elif resource.get('package_id'):
            # delete resources by package id
        #    query = "+pkg_id:\"%s\"" % (resource.get('package_id'))

        #bucket_indexer.delete_body(resource.get('id'))
        _i.handle_custom_indexing(operation=IndexOperationType.DELETE, obj=resource, obj_type="resource")
        indexer.delete_dict(query)

    def skip_index(self, resource):
        resource_jsonschema_type = _t.get_resource_type(resource)
        if _t.get_skip_indexing_from_registry(resource_jsonschema_type):
            log.warn('Skipping indexing for resource type {}'.format(
                resource_jsonschema_type))
            return True
        return False

# lets inject in the indexer
_INDICES.update({'resource': ResourceSearchIndex})

def _before_index_resource(organization, pkg_dict, resource):
    package_id = pkg_dict.get('id')
    resource_id =  resource.get('id')

    resource_jsonschema_opt = _t.get_resource_opt(resource)
    resource_jsonschema_type = _t.get_resource_type(resource)

    index_id = hashlib.md5(
        '%s%s' % (resource_id, config.get('ckan.site_id'))).hexdigest()
    tags = pkg_dict.get('tags')
    document = {
        'index_id': index_id,
        'revision_id': resource.get('revision_id'),
        'site_id': config.get('ckan.site_id'),
        'capacity': 'public' if pkg_dict.get('private') == False else 'private', # to be secure
        'permission_labels': _t.get_permission_labels(package_id),
        #'pkg_status': pkg_dict.get('status'), 
        #'status':resource.get('status'),
        'capabilities': [],
        'org_id': organization.get('id'),
        'org_name': organization.get('name'),
        'pkg_id': package_id,
        'pkg_tags':  [tag.get('name') for tag in tags] if tags else [],
        'pkg_name' : pkg_dict.get('name'),
        'pkg_title': pkg_dict.get('title'),
        'pkg_description' : pkg_dict.get('description'),
        'id': resource_id,
        'type': resource.get('resource_type'),
        'name': resource.get('name'),
        'description': resource.get('description'),
        'format': resource.get('format'),
        'url': resource.get('url'),
        # TODO 'owner' : who uploaded the resource
        'pkg_' + _c.SCHEMA_TYPE_KEY : _t.get_package_type(pkg_dict),
        _c.SCHEMA_TYPE_KEY: resource_jsonschema_type,
        _c.SCHEMA_BODY_KEY: json.dumps(_t.get_resource_body(resource)) or None,
        _c.SCHEMA_OPT_KEY:json.dumps( resource_jsonschema_opt),
        'indexed_ts': datetime.datetime.utcnow().isoformat()
    }
    # modify dates (SOLR is quite picky with dates, and only accepts ISO dates
    # with UTC time (i.e trailing Z)
    # See http://lucene.apache.org/solr/api/org/apache/solr/schema/DateField.html
    document['indexed_ts'] += 'Z'
    return document