# What is ckanext-jsonschema

This plugin provide a quite extensive set of functionalities to introduce **jsonschema based metadata types** into CKAN  (>=2.8) metadata.

It also provide an **User Interface** (UI) based on the [json-editor](https://github.com/json-editor/json-editor) library to properly **edit** and **validate** complex json fields.
The UI can also be customized in terms of components and autocompletion capabilities via javascript modules.

Provide an iso19139 implementation and a simplified profile and several types already implemented.

Provide several extension points to introduce new dataset and resource formats in other (json based) formats (f.e. STAC).


## How it works

Leveraging on the extra fields capabilities of CKAN, the jsonschema plugin is defining three extra predefined and fixed custom fields.

These will be used extensively to add json and jsonschema capabilities to CKAN.

The goal is to simplify the way a metadata mapping is performed in CKAN. 

These fields could be attached to the dataset, resource or view objects of CKAN.

## Basic functionalities :

Registry : 

This functionality drives the behaviours of jsonschema plugin. Each plugin  can define its own registry to define its own types with configurations.

In case you need more details please reference the [Registry section in development documentation](README_DEV.md#registry)

Validator :

This functionality of jsonschema plugin makes it possible to dynamically validate user inputs.

In case you need more details please reference the [Validator section in development documentation](README_DEV.md#validator)

View Configurations : 

This functionality allows each plugin to have multiple view types.

In case you need more details please reference the [View Configurations section in development documentation](README_DEV.md#view-configurations)

<a id="additional-functionalities"></a>
## ADDITIONAL FUNCTIONALITIES

Json addition is just a way to store JSON's in CKAN providing an interface and a validation. On top of these feature jsonschema implements some additional functionalities:

Extractor:

Jsonschema creates an extraction flow from JSON to CKAN or reverse

In case you need more details please reference the [Extractor section in development documentation](README_DEV.md#extractor)

Importer:

This functionality can be used to import metadata from an external repository, it can be in any supported format (f.e. from geonetwork).

In case you need more details please reference the [Importer section in development documentation](README_DEV.md#importer)

Exporter:

This functionality can be used to export a metadata in CKAN to a standart XML.

In case you need more details please reference the [Exporter section in development documentation](README_DEV.md#exporter)

Cloner:

This functionality provides an interface to duplicate an existing metadata.

In case you need more details please reference the [Cloner section in development documentation](README_DEV.md#cloner)

Harvester:
<!-- TODO -->

Resolver:

This functionality allows a change in a CKAN object to be reflected into another object that is configured on it. 

In case you need more details please reference the [Resolver section in development documentation](README_DEV.md#resolver)

Wrapper:

This functionality allows users to compose views into a big collection.

In case you need more details please reference the [Wrapper section in development documentation](README_DEV.md#wrapper)



### Views

From the jsonschema perspective, the view is the representation of the metadata. Each change in the metadata may be reflected into every view configured on it.

For example, changing the title or the description of the metadata, could cause a change in the appearance of the view.




# Installation and Administration 

The jsonschema plugin comes with several plugins. To add their functionalities, these must be configured in the *ckan.plugins* property. 

The plugin which depend on jsonschema can be of two types; they can implement **metadata** (and resource) functionalities or **view** functionalities.

## Plugins overview

| Plugin                  | Version  | Type     | Functionalities                                              |
| ----------------------- | -------- | -------- | :----------------------------------------------------------- |
| jsonschema              | Released | core     | This is the main plugin. It implements several actions related to jsonschema metadata, blueprints and Jinja helpers. It also implements the custom jsonschema indexing for SOLR. This plugin needs to be configured to use any of the following |
| jsonschema_iso          | Released | metadata | Adds support for the *iso* metadata format and for different custom resources |
| jsonschema_stac         | Alpha    | metadata | Adds support for the *stac* metadata format and for the *stac-asset* resource |
| jsonschema_dataset      | Released | metadata | Needed to integrate the jsonschema functionalities with the dataset metadata. It also adds the *JSON* custom resource |
| jsonschema_frictionless | Alpha    | metadata | Adds support for *Tabular Data* custom resource              |
| harvester_iso19139      | Alpha    | metadata | Harvester for iso19139 from GeoNetwork using CSW. Superseded by the importer |


## Installation 

### SOLR

2 new solr cores should be created with names "ckan_resources" and "ckan_views" with schemas below.

-- TODO provide final versions of schemas and provide a way to create the cores--

Once the cores are created there will be 3 cores named {DEFAULT_CORE}, "ckan_resources" and "ckan_views"

Since {DEFAULT CORE} ,ckan default core name, may vary the following configuration should be added. 
```
ckanext.jsonschema.solr.default.core_name={DEFAULT_CORE}
```

And make sure solr_url is configured correctly

```
solr_url={SOLR_URL}/solr/{DEFAULT_CORE}
```

Note: {DEFAULT_CORE} may be an empty string, in that case the configuration should be: 

```
solr_url={SOLR_URL}/solr
```

As it already is in ckan


Tomcat9 solr:

Due to relaxedQueryPath limits (https://tomcat.apache.org/tomcat-8.5-doc/config/http.html)
we need to properly setup the connector:
nano /etc/tomcat9/server.xml

Setup the connector as following:

```xml
<Connector port="8983" protocol="HTTP/1.1"
                   connectionTimeout="20000"
               redirectPort="8443" relaxedQueryChars="&quot;&lt;&gt;[\]^`{|}"
/>
```

see also:

    "&quot;&lt;&gt;![\]^`{|}"


## Related plugins

| Plugin                    | Version  | Type     | Functionalities | Ref                                                        |
| ------------------------- | -------- | -------- | :-------------- | ---------------------------------------------------------- |
| terriajs                  | Released | view     |                 | https://bitbucket.org/cioapps/ckanext-terriajs             |
| jsonschema_dashboard      | Released | metadata |                 | https://bitbucket.org/cioapps/ckanext-jsonschema-dashboard |
| jsonschema_dashboard_view | Released | view     |                 | https://bitbucket.org/cioapps/ckanext-jsonschema-dashboard |


# API

## Using CKAN's REST API to Manage Data

This guide will walk you through using CKAN's REST API to perform essential data management tasks such as creating packages, resources, and views.


### Create a Package

To create a package using CKAN's REST API, you can use the following Python code as a starting point:

```python
import requests

# Define your CKAN API URL and API key
api_url = "https://{CKAN_URL}/api/action/package_create"
api_key = "your-api-key"

# Data for creating a new package
data = {
    "name": "my-new-package",
    "title": "My New Package",
    "author": "Your Name",
    # Add more metadata fields as needed
}

headers = {
    "Authorization": api_key,
    "Content-Type": "application/json",
}

response = requests.post(api_url, json=data, headers=headers)

```

### Create a Resource

```python

# Define your CKAN API URL and API key
api_url = "https://{CKAN_URL}/api/action/resource_create"
api_key = "your-api-key"

# Data for creating a new resource
data = {
    "package_id": "your-package-id",
    "name": "my-new-resource",
    "url": "https://url-to-your-data-resource",
    "format": "CSV",
    # Add more metadata fields as needed
}

headers = {
    "Authorization": api_key,
    "Content-Type": "application/json",
}

response = requests.post(api_url, json=data, headers=headers)

```
### Create a View

```python

# Define your CKAN API URL and API key
api_url = "https://{CKAN_URL}/api/action/resource_view_create"
api_key = "your-api-key"

# Data for creating a new view
data = {
    "resource_id": "your-resource-id",
    "title": "My Data View",
    "view_type": "recline_view",
    # Add more view configuration as needed
}

headers = {
    "Authorization": api_key,
    "Content-Type": "application/json",
}

response = requests.post(api_url, json=data, headers=headers)

```

## SOLR SEARCH API's

These 3 API's expose solr search on each solr index for packages, resources and views respectively.

The solr search parameters are directly entered as request parameters and passed to solr.


To see valid [valid solr search parameters](https://solr.apache.org/guide/6_6/common-query-parameters.html)


q(query) parameter can even be used to perform full-text search in all fields.

#### Request type:
	
GET

### Package Search
https://{CKAN_URL}/api/action/jsonschema_search_packages?{SOLR_SEARCH_PARAM1=SEARCH_EXP1&SOLR_SEARCH_PARAM2=SEARCH_EXP2, ...}

Example: https://{CKAN_URL}/api/action/jsonschema_search_packages?q=technology&fq=extras_jsonschema_type:iso&rows=5&sort=indexed_ts+ASC

#### Most used fields to search:
|Param|Type|Note|Example|
|--|--|--|--|
| id | String | package id |  |
| type | String | package type |  |
| name | String | package name |  |
| title | String | package title |  |
| description | String | package description |  |
| url | String | resource url |  |
| extras_jsonschema_type | String | package jsonschema type | "wms" |
| extras_jsonschema_body | String | package jsonschema body  |  |
| extras_jsonschema_opt | String | package jsonschema opt |  |
| res_name | List |  |  |
| res_description | List |  |  |
| res_format | List |  |  |
| res_url | List |  |  |
| res_type | List |  |  |
| organization | String |  |  |
| capacity | String | publish state of metadata | "private" or "public" |

To see the [full list of fields](https://raw.githubusercontent.com/ckan/ckan/2.8/ckan/config/solr/schema.xml) 

### Resource Search
https://{CKAN_URL}/api/action/jsonschema_search_resources?{SOLR_SEARCH_PARAM1=SEARCH_EXP1&SOLR_SEARCH_PARAM2=SEARCH_EXP2, ...}

Example: https://{CKAN_URL}/api/action/jsonschema_search_resources?q=global&fq=format:wms+AND+capacity:public


#### Acceptable fields to search:
|Param|Type|Note|Example|
|--|--|--|--|
| id | String | resource id |  |
| type | String | resource type |  |
| name | String | resource name |  |
| description | String | resource description |  |
| format | String | resource format | "online-resource" |
| url | String | resource url |  |
| jsonschema_type | String | resource jsonschema type | "wms" |
| jsonschema_body | String | resource jsonschema body  |  |
| jsonschema_opt | String | resource jsonschema opt |  |
| pkg_id | String |  |  |
| pkg_tags | List |  |  |
| pkg_name | String |  |  |
| pkg_title | String |  |  |
| pkg_description | String |  |  |
| pkg_jsonschema_type | String |  | "iso" |
| org_id | String |  |  |
| org_name | String |  |  |
| capacity | String | publish state of metadata | "private" or "public" |


### View Search
https://{CKAN_URL}/api/action/jsonschema_search_views?{SOLR_SEARCH_PARAM1=SEARCH_EXP1&SOLR_SEARCH_PARAM2=SEARCH_EXP2, ...}

Example: https://{CKAN_URL}/api/action/jsonschema_search_resources?q=\*:\*&fq=pkg_id:{package_id1}+OR+pkg_id:{package_id2}&facet=true&facet.field=res_format

It is even possible to use advanced query parameters like facet to obtain meta-metadata


#### Acceptable fields to search:
|Param|Type|Note|Example|
|--|--|--|--|
| id | String | view id |  |
| type | String | view type |  |
| name | String | view name |  |
| title | String | view title |  |
| description | String | viw description |  |
| jsonschema_type | String | view jsonschema type | "wms" |
| jsonschema_body | String | substrings can be search in resolved jsonschema body of view |  |
| jsonschema_opt | String | view jsonschema opt |  |
| res_id | String |  |  |
| res_type | String |  |  |
| res_name | String |  |  |
| res_description | String |  |  |
| res_format | String |  |  |
| res_url | String |  |  |
| res_jsonschema_type | String |  | "wms" |
| pkg_id | String |  |  |
| pkg_tags | String |  |  |
| pkg_name | String |  |  |
| pkg_title | String |  |  |
| pkg_description | String |  |  |
| pkg_jsonschema_type | String |  | "iso" |
| org_id | String |  |  |
| org_name | String |  |  |
| capabilities | List |  | ["analysis"] |
| capacity | String | publish state of metadata | "private" or "public" |
| site_id | string | ckan site id |  |

**Additional notes**

The permissions checks are added to the request behind the scenes so that the privacy of any piece of metadata is respected.


When no parameter is provided, "q=*:*" is added by default to have a successful response with no additional filtering rather than authorizations.


#### Response fields

Responses contain each and every non-empty search fields and the following ones

|Param|Type|Note|
|--|--|--|
| index_id | String | can also be used as search field when known |
| indexed_ts | List |  |



## View List

    https://{CKAN_URL}/api/action/jsonschema_view_list?package_id={VIEW_TYPE}

This api will try to return all the jsonschema based VIEWS indexed into solr from a specific package

It will only query for Public metadata
 
### Request type:
	
GET

### Mandatory params:
|Param|Type|Note|Example|
|--|--|--|--|
| package_id  | String | The ID or the Name of the package |  |

### Response:
|Param|Type|Note|Example|
|--|--|--|--|
| package_id  | String | ID of the package |  |
| resource_id  | String | ID of the resource |  |
| view_id  | String | ID of the view |  |
| metadata_link  | String (url) | WEB page url |  |
| resource_link  | String (url) | WEB page url |  |
| jsonschema_body_link | String (url) | REST API |  |
| view_type  | String | may match the **type** parameter | terriajs |
| jsonschema_body | String (Json) | Resolved view body | {"key":"value"} |
| jsonschema_type | String | may match the schema used from the registry, see below | wms |
| jsonschema_opt | String (Json) | meta-metadata optional informations, should never be exposed but can ship some hints |  |

**Notes:**
 - In case of spaces values please quote the string with "value with spaces"
 - In case of full you could try to use star notation but it's not guarantee a full text search (f.e.: "*value with *")

### Jsonschema Reload

Reload jsonschema after a change has been made to make effect come out.

    https://{CKAN_URL}/api/action/jsonschema_reload


### Geospatial search:

Yes we have also bbox indices fetched directly from wms services... (provided by terriajs view plugin) but we are still not able to use the BBOX parameters which are in solr.
TODO we planned to change the solr bbox index leveraging on solr > 4.x



## Body, Type, Opt

These are GET and sideffect free calls which can be used to inspect the content of the fields controlled by the 

    https://{CKAN_URL}/jsonschema/{body|type|opt}/{PACKAGE_ID}[/{RESOURCE_ID}[/{VIEW_ID}]]

## Clone (webpage)

    https://{CKAN_URL}/jsonschema/clone

## Importer (webpage)

    https://{CKAN_URL}/jsonschema/importer

## Validate (webpage)

    https://{CKAN_URL}/jsonschema/validate


## Registry

Returns all the available (registered) jsonschema types

    https://{CKAN_URL}/jsonschema/registry[/jsonschema_type]
 
### Request type:
	
GET

## Schema, template and javascript modules

For each registry entry you may want to retrieve the **schema**, the **template** or the **javascript modules**

    https://{CKAN_URL}/jsonschema/{schema|template|module}/jsonschema_type_path.{json|js}

The javascript **module** is loaded into the interface and can be used to provide autocompletion for a specific jsonschema_type and for several othe extensions point (ref to the json-editor library for a list)

The **template** is used to populate an empty type when it is created

The **schema** is the jsonschema definition of the associated type


### Request type:
	
GET



## View specific params

When you have a view based on the jsonschema plugin you may (it depends on the specific implementation) have some additional arguments
available:

https://{CKAN_URL}/jsonschema/{body|type|opt}/{PACKAGE_ID}/{RESOURCE_ID}/{VIEW_ID}

To understand the following optional parameter we may describe the concept of item and the resolution:

### wrap

wrap=true is a simple way to ask to the implementing plugin to return a **config** (potentially dynamically generated) starting from an **item**. 

In case you need more details please reference the [Additional Functionalities section](#additional-functionalities)


So if your view is defining an **item** and the url is the following:

    https://{CKAN_URL}/jsonschema/body/{PACKAGE_ID}/{RESOURCE_ID}/{VIEW_ID_OF_THE_ITEM}

To obtain a config to pass to your application viewer you may pass:

    https://{CKAN_URL}/jsonschema/body/{PACKAGE_ID}/{RESOURCE_ID}/{VIEW_ID_OF_THE_ITEM}?wrap=true

Which may wrap the view (body) of the **item** you are referring to into a fake or dynamically generated **config** that your application will recognize as valid.

Obviously all of the above is totally optional if the implementing plugin and the viewer do not need to group items.

This is instead largely used by the terriajs and the Dashboard plugins.

### resolve

resolve=true is a way to ask a change in a CKAN object to be reflected into another object that is configured on it.
 more details please reference the [Additional Functionalities section](#additional-functionalities)

For example if a view jsonschema_body contains:

```json
{
    "name":"{{resource.name}}",
    "description": "{{package.notes or resource.description}}"
}
```

Using the resolve=true parameter the expected returned body will be:

https://{CKAN_URL}/jsonschema/body/{PACKAGE_ID}/{RESOURCE_ID}/{VIEW_ID_OF_THE_ITEM}?resolve=true


```json
{
    "name":"The name of the resource",
    "description": "The description of the package (or the one from the resource)"
}
```


### force_resolve

To enforce the resolution of an already resolved view, use this parameter.

### Acceptable params:
|Param|Type|Note|Example|
|--|--|--|--|
| wrap | Boolean | Is used to provide a default wrapping structure when the item is not the first  | resolve=False |
| resolve | Boolean |  | resolve=True |
| force_resolve | Boolean |  | force_resolve=True |



# Provided plugins:

## jsonschema

Base plugin to enable extensions points provide basic jsonschema functionnalities

## jsonschema_iso19139

Extension to provide an iso19139 binding from iso19139 and the below simplified iso profile

## jsonschema_iso

Extension to provide a simplified but quite complex iso model

## harvester_iso19139 (deprecated, ise importer)

An harvester from CSW to iso19139/iso requires harves plugin to be installed, also has a dedicate requirements file

## Suggested configuration:

    jsonschema jsonschema_iso19139 jsonschema_iso

Known issue:
---
The xml importer is encountering some runtime issue (due to xml format body) with the google analytics extension.